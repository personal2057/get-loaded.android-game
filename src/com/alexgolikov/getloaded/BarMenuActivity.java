package com.alexgolikov.getloaded;

import java.util.ArrayList;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.BufferType;


/*
 * 	Activity for showing list of drinks (Bar Menu)
 */

public class BarMenuActivity extends Activity {

	private Typeface ttf;
	private int textsize = 0;
	
	private BarMenuAdapter adapter;
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
    	super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);   
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        setContentView(R.layout.bar_menu_list);   
        ttf = HardPartLoader.tf;//Typeface.createFromAsset(getAssets(), "fonts/KOMIKAX_.ttf");  
        textsize = this.getIntent().getExtras().getInt("textsize");
        
        ListView listview = (ListView) findViewById(R.id.listview_bar_menu);

        
        adapter = new BarMenuAdapter(this, HardPartLoader.getDrinksMassive());
        
        listview.setAdapter(adapter);
        listview.setVerticalFadingEdgeEnabled(false);
        listview.setScrollbarFadingEnabled(false);
        
        //button back
        TextView text_back = (TextView) findViewById(R.id.back_bar_menu);
        text_back.setTextSize(textsize/3);
        text_back.setTypeface(ttf);
        text_back.setBackgroundColor(Color.TRANSPARENT);
        
        text_back.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				finish();
			}
		});
		
    }
    
    
    
    private class BarMenuAdapter extends BaseAdapter {

    	private ArrayList<Drink> drinks;
    	private LayoutInflater lInflater;
    	
    	private TextView curTextView;
    	private Spannable span;
    	
    	private StringBuilder short_title = new StringBuilder();
    	
        BarMenuAdapter(Context context, ArrayList<Drink> drinks) {        
        	this.drinks = drinks;
        	lInflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE); 	
        }
        
        
        
        public View getView(int position, View convertView, ViewGroup parent) {
        
        	View view = convertView;
        	
        	if (view == null) {
        		view = lInflater.inflate(R.layout.bar_menu_item_1, parent, false);
        	}
        	
        	Drink current_drink = getItem(position);
        	view.setMinimumHeight(HardPartLoader.beerImage[0].getHeight());
        	
        	//special way of drawing if it's a secret drink and it's not being unlocked
        	if ((current_drink.isSecretDrink()) && (!SecretDrinksManager.checkSecretDrink(current_drink.getId()))){
    			((ImageView) view.findViewById(R.id.drinkImage)).setImageBitmap(current_drink.getDrinkImageBlank());
    			curTextView = ((TextView) view.findViewById(R.id.drink_name));
    			curTextView.setText("Secret Drink");
    			curTextView.setTypeface(ttf);
    			curTextView.setTextSize(textsize/6);
    			curTextView.setTextColor(getResources().getColor(R.color.plain_string_2));
    			curTextView.setShadowLayer(0.6f, 1.5f, 1.5f, getResources().getColor(R.color.shadow_color));
    	
    			//short_title.setLength(0);
    			//if (current_drink.getStrength() < 10) short_title.append("Alcohol by volume:  "); else short_title.append("Alcohol by volume: ");
    			curTextView = ((TextView) view.findViewById(R.id.abv));
    			//curTextView.setText(short_title + Integer.toString(Math.round(current_drink.getStrength())) + "%", BufferType.SPANNABLE);
    			//span = (Spannable) curTextView.getText();
    			//span.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.plain_string_2)), 0, "Alcohol by volume:".length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
    			curTextView.setText("Get " + Integer.valueOf(SecretDrinksManager.SECRET_DRINK_1_BORDER) + " points to unlock");
    			curTextView.setTypeface(ttf);
    			curTextView.setTextSize(textsize/6);
    			curTextView.setTextColor(getResources().getColor(R.color.plain_string));
    			curTextView.setShadowLayer(0.6f, 1.5f, 1.5f, getResources().getColor(R.color.shadow_color));
    	
    			curTextView = ((TextView) view.findViewById(R.id.points));
    			curTextView.setText("");
    			//curTextView.setText("Points: " + Integer.toString(current_drink.getPoints()), BufferType.SPANNABLE);
    			//span = (Spannable) curTextView.getText();
    			//span.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.plain_string_2)), 0, "Points:".length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
    			//curTextView.setText(short_title);
    			//curTextView.setTypeface(ttf);
    			//curTextView.setTextSize(textsize/6);
    			//curTextView.setTextColor(getResources().getColor(R.color.plain_string));
    			//curTextView.setShadowLayer(0.6f, 1.5f, 1.5f, getResources().getColor(R.color.shadow_color));
    	
    			///int pointsBorder = 
    			curTextView = ((TextView) view.findViewById(R.id.description));
    			curTextView.setText("");
    			//curTextView.setText("Get " + " " + "to unlock this drink");
    			//curTextView.setText(current_drink.getDescription());
    			//curTextView.setTypeface(ttf);
    			//curTextView.setTextSize(textsize/8);
    			//curTextView.setTextColor(getResources().getColor(R.color.description));
    			//curTextView.setShadowLayer(0.6f, 1.5f, 1.5f, getResources().getColor(R.color.shadow_color));
        	}
        		else{
        			((ImageView) view.findViewById(R.id.drinkImage)).setImageBitmap(current_drink.getBitmap());
        			curTextView = ((TextView) view.findViewById(R.id.drink_name));
        			curTextView.setText("Drink: " + current_drink.getName(), BufferType.SPANNABLE);
        			span = (Spannable) curTextView.getText();
        			span.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.plain_string_2)), 0, "Drink:".length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        			curTextView.setTypeface(ttf);
        			curTextView.setTextSize(textsize/6);
        			curTextView.setTextColor(getResources().getColor(R.color.plain_string));
        			curTextView.setShadowLayer(0.6f, 1.5f, 1.5f, getResources().getColor(R.color.shadow_color));
        	
        			short_title.setLength(0);
        			if (current_drink.getStrength() < 10) short_title.append("Alcohol by volume:  "); else short_title.append("Alcohol by volume: ");
        			curTextView = ((TextView) view.findViewById(R.id.abv));
        			curTextView.setText(short_title + Integer.toString(Math.round(current_drink.getStrength())) + "%", BufferType.SPANNABLE);
        			span = (Spannable) curTextView.getText();
        			span.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.plain_string_2)), 0, "Alcohol by volume:".length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        			curTextView.setTypeface(ttf);
        			curTextView.setTextSize(textsize/6);
        			curTextView.setTextColor(getResources().getColor(R.color.plain_string));
        			curTextView.setShadowLayer(0.6f, 1.5f, 1.5f, getResources().getColor(R.color.shadow_color));
        	
        			curTextView = ((TextView) view.findViewById(R.id.points));
        			curTextView.setText("Points: " + Integer.toString(current_drink.getPoints()), BufferType.SPANNABLE);
        			span = (Spannable) curTextView.getText();
        			span.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.plain_string_2)), 0, "Points:".length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        			curTextView.setTypeface(ttf);
        			curTextView.setTextSize(textsize/6);
        			curTextView.setTextColor(getResources().getColor(R.color.plain_string));
        			curTextView.setShadowLayer(0.6f, 1.5f, 1.5f, getResources().getColor(R.color.shadow_color));
        	
        			curTextView = ((TextView) view.findViewById(R.id.description));
        			curTextView.setText(current_drink.getDescription());
        			curTextView.setTypeface(ttf);
        			curTextView.setTextSize(textsize/8);
        			curTextView.setTextColor(getResources().getColor(R.color.description));
        			curTextView.setShadowLayer(0.6f, 1.5f, 1.5f, getResources().getColor(R.color.shadow_color));
        		}
        	
        	return view;
        }
        
        
        
        public int getCount() {
        	return this.drinks.size();
        }
        
        
        
        public Drink getItem(int position) {
        	return this.drinks.get(position);
        }

        
        
        public long getItemId(int position) {
        	return position;
        }
        
        
        @Override
        public boolean isEnabled(int position){   	
        	return false;
        }
        
        
        @Override
        public boolean areAllItemsEnabled(){
        	
        	return false;
        	
        }
        
        
        @Override
        public boolean hasStableIds() {
        	
        	return true;
        	
        }

      }
    

    
}
