package com.alexgolikov.getloaded;

import java.lang.ref.PhantomReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.Layout.Alignment;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Log;
import android.util.SparseArray;




public class EventManager  {
	
	
	//private static HashMap<Integer, String> achievementsLabelList = new HashMap<Integer, String>();
	//private static HashMap<Integer, String> achievementsLabelList2 = new HashMap<Integer, String>();	
	private static SparseArray<String> achievementsLabelList = new SparseArray<String>();
	
	private static ArrayList<String> proDrinkerPhrasesWelcome = new ArrayList<String>();
	private static ArrayList<String> proDrinkerPhrasesAdvise = new ArrayList<String>();
	private static ArrayList<String> proDrinkerPhrasesJoke = new ArrayList<String>();
	private static ArrayList<String> proDrinkerPhrasesPraise = new ArrayList<String>();
	private static ArrayList<String> proDrinkerPhrasesBeer = new ArrayList<String>();
	private static ArrayList<String> proDrinkerPhrasesWine = new ArrayList<String>();
	private static ArrayList<String> proDrinkerPhrasesTequila = new ArrayList<String>();
	private static ArrayList<String> proDrinkerPhrasesWhiskey = new ArrayList<String>();
	private static ArrayList<String> proDrinkerPhrasesVodka = new ArrayList<String>();
	
	//time between same achievements is msec
	private static final int ACHIEVEMENTS_PAUSE_TIME = 30; 
	//time between "prodrinker" events in sec
	private static final long PRODRINKER_PAUSE_TIME = 14;
	private static final int PRODRINKER_CHANCE_OF_SHOWING = 3; //of 10, 3 - best way
	
	public static final int OCTOBERFEST_IN_YOUR_HEART = 0; //achievement: 5 beers in a row
	public static final int STAY_FOR_A_NIGHT = 1; //achievement: 10 minutes in game
	public static final int BE_STRONG = 2; // achievement: 3 minutes without intoxication
	public static final int ZDRAVSTVUY_DOROGOY = 3; // achievement: 5 shots of vodka in a row
	public static final int MACHINEGUN = 4; // achievement: drinks faster then 1 drink in second
	public static final int ARRIBA = 5; // achievement: 5 shots of tequila in a row
	public static final int BOILERMAKER = 6; // achievement: drinks only vodka and beer
	public static final int ON_EDGE = 7; // achievement: be near a loose border
	
	
	static{
		
		//Don't forget to place char "!" after the first sentence	
		achievementsLabelList.clear();
		achievementsLabelList.put(OCTOBERFEST_IN_YOUR_HEART, "Visit Octoberfest! Beer lover has been found");
		achievementsLabelList.put(STAY_FOR_A_NIGHT, "Stay here for a night! Waste your time in the bar");
		achievementsLabelList.put(BE_STRONG, "Be strong! Hello, alco-ninja!");
		achievementsLabelList.put(ZDRAVSTVUY_DOROGOY, "Zdravstvuy, dorogoy! Na zdorovje!");
		achievementsLabelList.put(MACHINEGUN, "Show me a machinegun! Drink non-stop and TRA-TA-TA!");
		achievementsLabelList.put(ARRIBA, "Andale, Andale! Ariba, Ariba! Muy bien!");
		achievementsLabelList.put(BOILERMAKER, "Try our Boilermaker! Catch up the hardest punch");
		achievementsLabelList.put(ON_EDGE, "On edge! You are almost loaded");
	}
	
	
	public ArrayList<Event> eventAchieveMass;
	public ArrayList<Event> eventAdviceMass;
	
	long nowTime = 0;
	private int achivementMachinegunTime = 0;
	private long achivementBoilermakerTime = 0;
	private boolean achievementOnEdge = false;
	private long prodrinkerLastShowTime = 0;
	
	ScreenManager screenManager;
	Context context;
	Random rand;
	MySQLiteHelper achievementsBase;
	
	EventManager(Context context, ScreenManager screenManager){
		
		eventAchieveMass = new ArrayList<Event>();
		eventAdviceMass = new ArrayList<Event>();
		this.screenManager = screenManager;
		this.context = context;
		rand = new Random();
		achievementOnEdge = false;
		achievementsBase = new MySQLiteHelper(context);
		
		proDrinkerPhrasesAdvise.clear();
		proDrinkerPhrasesAdvise.add(context.getResources().getString(R.string.pro_drinker_advise1));
		proDrinkerPhrasesAdvise.add(context.getResources().getString(R.string.pro_drinker_advise2));
		proDrinkerPhrasesAdvise.add(context.getResources().getString(R.string.pro_drinker_advise3));
		proDrinkerPhrasesAdvise.add(context.getResources().getString(R.string.pro_drinker_advise4));
		proDrinkerPhrasesAdvise.add(context.getResources().getString(R.string.pro_drinker_advise5));
		proDrinkerPhrasesAdvise.add(context.getResources().getString(R.string.pro_drinker_advise6));
		proDrinkerPhrasesAdvise.add(context.getResources().getString(R.string.pro_drinker_advise7));
		proDrinkerPhrasesAdvise.add(context.getResources().getString(R.string.pro_drinker_advise8));
		proDrinkerPhrasesAdvise.add(context.getResources().getString(R.string.pro_drinker_advise9));
		proDrinkerPhrasesAdvise.add(context.getResources().getString(R.string.pro_drinker_advise10));
		
		proDrinkerPhrasesJoke.clear();
		proDrinkerPhrasesJoke.add(context.getResources().getString(R.string.pro_drinker_joke1));
		proDrinkerPhrasesJoke.add(context.getResources().getString(R.string.pro_drinker_joke2));
		proDrinkerPhrasesJoke.add(context.getResources().getString(R.string.pro_drinker_joke3));
		proDrinkerPhrasesJoke.add(context.getResources().getString(R.string.pro_drinker_joke4));
		proDrinkerPhrasesJoke.add(context.getResources().getString(R.string.pro_drinker_joke5));
		proDrinkerPhrasesJoke.add(context.getResources().getString(R.string.pro_drinker_joke6));
		proDrinkerPhrasesJoke.add(context.getResources().getString(R.string.pro_drinker_joke7));
		proDrinkerPhrasesJoke.add(context.getResources().getString(R.string.pro_drinker_joke8));
		proDrinkerPhrasesJoke.add(context.getResources().getString(R.string.pro_drinker_joke9));
		proDrinkerPhrasesJoke.add(context.getResources().getString(R.string.pro_drinker_joke10));
		proDrinkerPhrasesJoke.add(context.getResources().getString(R.string.pro_drinker_joke11));
		proDrinkerPhrasesJoke.add(context.getResources().getString(R.string.pro_drinker_joke12));
		proDrinkerPhrasesJoke.add(context.getResources().getString(R.string.pro_drinker_joke13));
		proDrinkerPhrasesJoke.add(context.getResources().getString(R.string.pro_drinker_joke14));
		proDrinkerPhrasesJoke.add(context.getResources().getString(R.string.pro_drinker_joke15));
		proDrinkerPhrasesJoke.add(context.getResources().getString(R.string.pro_drinker_joke16));
		
		proDrinkerPhrasesPraise.clear();
		proDrinkerPhrasesPraise.add(context.getResources().getString(R.string.pro_drinker_prise1));
		proDrinkerPhrasesPraise.add(context.getResources().getString(R.string.pro_drinker_prise2));
		proDrinkerPhrasesPraise.add(context.getResources().getString(R.string.pro_drinker_prise3));
		proDrinkerPhrasesPraise.add(context.getResources().getString(R.string.pro_drinker_prise4));
		proDrinkerPhrasesPraise.add(context.getResources().getString(R.string.pro_drinker_prise5));
		
		proDrinkerPhrasesWelcome.clear();
		proDrinkerPhrasesWelcome.add(context.getResources().getString(R.string.pro_drinker_hello1));
		proDrinkerPhrasesWelcome.add(context.getResources().getString(R.string.pro_drinker_hello2));
		proDrinkerPhrasesWelcome.add(context.getResources().getString(R.string.pro_drinker_hello3));
		proDrinkerPhrasesWelcome.add(context.getResources().getString(R.string.pro_drinker_hello4));
		proDrinkerPhrasesWelcome.add(context.getResources().getString(R.string.pro_drinker_hello5));
		
		proDrinkerPhrasesBeer.clear();
		proDrinkerPhrasesBeer.add(context.getResources().getString(R.string.pro_drinker_joke_beer1));
		proDrinkerPhrasesBeer.add(context.getResources().getString(R.string.pro_drinker_joke_beer2));
		proDrinkerPhrasesBeer.add(context.getResources().getString(R.string.pro_drinker_joke_beer3));

		proDrinkerPhrasesWine.clear();
		proDrinkerPhrasesWine.add(context.getResources().getString(R.string.pro_drinker_joke_wine1));
		proDrinkerPhrasesWine.add(context.getResources().getString(R.string.pro_drinker_joke_wine2));
		
		proDrinkerPhrasesTequila.clear();
		proDrinkerPhrasesTequila.add(context.getResources().getString(R.string.pro_drinker_joke_tequila1));
		proDrinkerPhrasesTequila.add(context.getResources().getString(R.string.pro_drinker_joke_tequila2));
		
		proDrinkerPhrasesVodka.clear();
		proDrinkerPhrasesVodka.add(context.getResources().getString(R.string.pro_drinker_joke_vodka1));
		proDrinkerPhrasesVodka.add(context.getResources().getString(R.string.pro_drinker_joke_vodka2));

		proDrinkerPhrasesWhiskey.clear();
		proDrinkerPhrasesWhiskey.add(context.getResources().getString(R.string.pro_drinker_joke_whiskey1));
		proDrinkerPhrasesWhiskey.add(context.getResources().getString(R.string.pro_drinker_joke_whiskey2));
	}
	
	
	public EventManager() {
		// blank constructor 
	}
	
	
	//get list of achievements titles for displaying as a list
	public ArrayList<String> getAchievementsLabelList(){
		
		ArrayList<String> list = new ArrayList<String>();

		for (int i = 0; i < achievementsLabelList.size(); i++)
			list.add(achievementsLabelList.get(i).substring(0, achievementsLabelList.get(i).indexOf('!')));
			
		return list;
		
	}
	
	
	
	private boolean checkDrinksCountForAchievement(ArrayList<Drink> drinksMass, int drinkIndex){
		
		int count = 0;
		
		if ((drinksMass.get(0).getId() != drinkIndex) || (drinksMass.size() < GameModel.DRINKS_COUNT_FOR_ACHIEVEMENT_CHECK) ||
			((drinksMass.size() > GameModel.DRINKS_COUNT_FOR_ACHIEVEMENT_CHECK) && (drinksMass.get(GameModel.DRINKS_COUNT_FOR_ACHIEVEMENT_CHECK).getId() == drinkIndex))) return false;
		else{
			for (int i = 0; i < drinksMass.size(); i++)
				if (drinksMass.get(i).getId() == drinkIndex) ++count;
				
			if (count == GameModel.DRINKS_COUNT_FOR_ACHIEVEMENT_CHECK) return true;
			else return false;
		}		
	}
	
	
	//check Machinegun achievement
	private boolean checkDrinksTimeForAchievement(ArrayList<Integer> timeMass){
		
		if (timeMass.size() >= GameModel.DRINKS_COUNT_FOR_ACHIEVEMENT_CHECK && (achivementMachinegunTime == 0 || timeMass.get(0) - achivementMachinegunTime > ACHIEVEMENTS_PAUSE_TIME)){
			long diff = 0;
		
			for (int i = 0; i < GameModel.DRINKS_COUNT_FOR_ACHIEVEMENT_CHECK-1; i++){
				diff = diff + (timeMass.get(i)-timeMass.get(i+1));
			}
		
			if (diff > 0 && diff < GameModel.DRINKS_COUNT_FOR_ACHIEVEMENT_CHECK-3){
				achivementMachinegunTime = timeMass.get(0);
				return true;
			}else return false;
		} 
		else return false;
	}
	
	
	//check Boilermaker achievement
	private boolean checkForBoilermakerAchievement(ArrayList<Drink> drinksMass){
		
		int vodkaCount = 0;
		int beerCount = 0;
		
		if (((drinksMass.get(0).getId() != Drink.BEER_DRINK) && (drinksMass.get(0).getId() != Drink.VODKA_DRINK)) 
				|| (drinksMass.size() < GameModel.DRINKS_COUNT_FOR_ACHIEVEMENT_CHECK) || (System.currentTimeMillis() - achivementBoilermakerTime < ACHIEVEMENTS_PAUSE_TIME * 1000))
			return false;
			else{
				for (int i = 0; i < drinksMass.size() - 1; i++){
					if (drinksMass.get(i).getId() == Drink.BEER_DRINK) ++beerCount;
					if (drinksMass.get(i).getId() == Drink.VODKA_DRINK) ++vodkaCount;
				}
				
				if ((vodkaCount + beerCount != GameModel.DRINKS_COUNT_FOR_ACHIEVEMENT_CHECK) || (beerCount < 2) || (vodkaCount < 2)) return false;
					else{
						achivementBoilermakerTime = System.currentTimeMillis();
						return true;
					}
			}		
	}

	//checking achievements in game (checking drank drinks)
	//@overload
	public void checkAchievmentEvent(ArrayList<Drink> drinksMass, ArrayList<Integer> timeMass){
		
		if (drinksMass.size() >= GameModel.DRINKS_COUNT_FOR_ACHIEVEMENT_CHECK){
			//check achievement "OCTOBERFEST_IN_YOUR_HEART"
			if (drinksMass.get(0).getId() == Drink.BEER_DRINK)
				if (checkDrinksCountForAchievement(drinksMass, Drink.BEER_DRINK)) addAchieveEvent(new AchieveEvent(achievementsLabelList.get(EventManager.OCTOBERFEST_IN_YOUR_HEART), EventManager.OCTOBERFEST_IN_YOUR_HEART, screenManager.getTextSize()/2, achievementsBase.checkAchievemntInDB(EventManager.OCTOBERFEST_IN_YOUR_HEART), true));
		
			//check achievement "ZDRAVSTVUY_DOROGOY"
			if (drinksMass.get(0).getId() == Drink.VODKA_DRINK)
				if (checkDrinksCountForAchievement(drinksMass, Drink.VODKA_DRINK)) addAchieveEvent(new AchieveEvent(achievementsLabelList.get(EventManager.ZDRAVSTVUY_DOROGOY), EventManager.ZDRAVSTVUY_DOROGOY, screenManager.getTextSize()/2, achievementsBase.checkAchievemntInDB(EventManager.ZDRAVSTVUY_DOROGOY), true));
		
			//check achievement "ARRIBA"
			if (drinksMass.get(0).getId() == Drink.TEQUILA_DRINK)
				if (checkDrinksCountForAchievement(drinksMass, Drink.TEQUILA_DRINK)) addAchieveEvent(new AchieveEvent(achievementsLabelList.get(EventManager.ARRIBA), EventManager.ARRIBA, screenManager.getTextSize()/2, achievementsBase.checkAchievemntInDB(EventManager.ARRIBA), true));
		
			//check achievement "BOILERMAKER"
			if (checkForBoilermakerAchievement(drinksMass)) addAchieveEvent(new AchieveEvent(achievementsLabelList.get(EventManager.BOILERMAKER), EventManager.BOILERMAKER, screenManager.getTextSize()/2, achievementsBase.checkAchievemntInDB(EventManager.BOILERMAKER), true));
		
			//check achievement "MACHINEGUN"
			if (checkDrinksTimeForAchievement(timeMass)) addAchieveEvent(new AchieveEvent(achievementsLabelList.get(EventManager.MACHINEGUN), EventManager.MACHINEGUN, screenManager.getTextSize()/2, achievementsBase.checkAchievemntInDB(EventManager.MACHINEGUN), true));
		}
	}
	
	
	//checking achievements, connected with time or intoxication
	//@Overload
	public void checkAchievmentEvent(int minutes, int seconds, Player player){
		
		//checking achievement "STAY FOR A NIGHT" (be in game 10 minutes)
		if (minutes == 15 && seconds == 0) addAchieveEvent(new AchieveEvent(achievementsLabelList.get(EventManager.STAY_FOR_A_NIGHT), EventManager.STAY_FOR_A_NIGHT, screenManager.getTextSize()/2, achievementsBase.checkAchievemntInDB(EventManager.STAY_FOR_A_NIGHT), true));
		
		//checking achievement "BE STRONG" (drink first 3 minutes without intoxication)
		if (minutes == 3 && seconds == 0 && player.getPoints() > 0 && player.getIntoxicationLevel() == 0) addAchieveEvent(new AchieveEvent(achievementsLabelList.get(EventManager.BE_STRONG), EventManager.BE_STRONG, screenManager.getTextSize()/2, achievementsBase.checkAchievemntInDB(EventManager.BE_STRONG), true));
		
		//checking achievement "ON EDGE" (be near loose border)
		if (!(achievementOnEdge) && Math.round(player.getIntoxicationLimit() - player.getIntoxicationLevel()) <= 10){
			achievementOnEdge = true;
			addAchieveEvent(new AchieveEvent(achievementsLabelList.get(EventManager.ON_EDGE), EventManager.ON_EDGE, screenManager.getTextSize()/2, achievementsBase.checkAchievemntInDB(EventManager.ON_EDGE), true));
		}
	}
	
	
	//check points for unlocking secret drinks
	public void checkPoints(int points){
		
		int id = SecretDrinksManager.checkPoints(points);
		
		switch(id){
			case Drink.SECRET_DRINK_1:{
				addAchieveEvent(new AchieveEvent(Drink.SECRET_DRINK_1_NAME, id, screenManager.getTextSize()/2, true, false));
				achievementsBase.addSecretDrinkInDB(id);
			}
			break;
		}
	}
	
	
	
	//random of showing "prodrinker" (every PRODRINKER_SHOW_TIME)
	public void checkProDrinkerEvent(int minutes, int seconds, Player player){
		long curTime = System.currentTimeMillis();
		
		if (curTime - prodrinkerLastShowTime > (PRODRINKER_PAUSE_TIME * 1000)){
			prodrinkerLastShowTime = curTime;
			
			if (rand.nextInt(10) < PRODRINKER_CHANCE_OF_SHOWING){
				String phrase = "";
				int chance = rand.nextInt(10);
				if (minutes == 0 && seconds <= PRODRINKER_PAUSE_TIME && player.getPoints() == 0) phrase = proDrinkerPhrasesWelcome.get(rand.nextInt(proDrinkerPhrasesWelcome.size()));
					else	
					if (chance >= 8) phrase = proDrinkerPhrasesAdvise.get(rand.nextInt(proDrinkerPhrasesAdvise.size()));
					else
						if (chance < 3) phrase = proDrinkerPhrasesPraise.get(rand.nextInt(proDrinkerPhrasesPraise.size()));
						else phrase = proDrinkerPhrasesJoke.get(rand.nextInt(proDrinkerPhrasesJoke.size()));
					
				addAdviceEvent(new ProDrinkerEvent(screenManager, phrase, screenManager.getTextSize()));
			}
		}	
	}
	
	//@Overload
	public void checkProDrinkerEvent(Drink drink){
		long curTime = System.currentTimeMillis();
		
		if (curTime - prodrinkerLastShowTime > ((PRODRINKER_PAUSE_TIME * 1000)/2)){
			
			if (rand.nextInt(10) < PRODRINKER_CHANCE_OF_SHOWING){
				prodrinkerLastShowTime = curTime;
				String phrase = "";
				
				switch (drink.getId()){
					
					case Drink.BEER_DRINK: phrase = proDrinkerPhrasesBeer.get(rand.nextInt(proDrinkerPhrasesBeer.size()));
					break;
					
					case Drink.WINE_DRINK: phrase = proDrinkerPhrasesWine.get(rand.nextInt(proDrinkerPhrasesWine.size()));
					break;
					
					case Drink.TEQUILA_DRINK: phrase = proDrinkerPhrasesTequila.get(rand.nextInt(proDrinkerPhrasesTequila.size()));
					break;
					
					case Drink.VODKA_DRINK: phrase = proDrinkerPhrasesVodka.get(rand.nextInt(proDrinkerPhrasesVodka.size()));
					break;
					
					/*
					case Drink.WHISKEY_DRINK: phrase = proDrinkerPhrasesWhiskey.get(rand.nextInt(proDrinkerPhrasesWhiskey.size()));
					break;
					 */
					
				}
					
				if (phrase.length() > 0) addAdviceEvent(new ProDrinkerEvent(screenManager, phrase, screenManager.getTextSize()));
			}
		}	
	}
	
	
	
	
	public void addAchieveEvent(Event event){
		eventAchieveMass.add(event);
	}
	
	public void addAdviceEvent(Event event){
		eventAdviceMass.add(event);
	}
	
	
	public void clearEventMassives(){
		eventAchieveMass.clear();
		eventAdviceMass.clear();
	}
	
	public void drawEvents(Canvas canvas){
		
		nowTime = System.currentTimeMillis();
		
		if (!eventAchieveMass.isEmpty()){		
			if ((nowTime - eventAchieveMass.get(0).getStartTime()) < eventAchieveMass.get(0).getShowingTime()){
				eventAchieveMass.get(0).drawEvent(canvas, screenManager.getScreenWidth()/2, screenManager.getScreenHeight()/2, screenManager.getAnimationTime());}
			else{
				eventAchieveMass.remove(0);
			}
		}
		
		if (!eventAdviceMass.isEmpty()){		
			if ((nowTime - eventAdviceMass.get(0).getStartTime()) < eventAdviceMass.get(0).getShowingTime()){
				eventAdviceMass.get(0).drawEvent(canvas, screenManager.getScreenWidth()/2, screenManager.getScreenHeight()/2, screenManager.getAnimationTime());}
			else{
				eventAdviceMass.remove(0);
			}
		}
	}
	
}







interface Event{
	
	long showingTime = 5000;
	long startTime = 0;
	String text = "";
	
	final int ACHIEVE_EVENT = 1;
	final int ADVICE_EVENT = 2;
	
	int id = 0;

	public long getShowingTime();	
	public String getText();	
	public long getStartTime();
	public int getId();
	
	void drawEvent(Canvas canvas, int x, int y, int animationTime);
}








class ProDrinkerEvent implements Event{
	
	//private Paint paint;
	private TextPaint paint = new TextPaint();
	private long startTime = 0;
	
	private int id = 2;
	
	private int xShift = 0;
	private int xBorder = 0;
	final private long SHOWING_TIME = 6000; //msec
	final private float TEXT_SIZE_REDUCER = 0.3f;
	
	private String text;
	
	//coordinates of pro drinker picture with chat box opening
	private int x1;
	//coordinates of chat box ending
	private int x3;
	//step for drawing picture 2
	private int x2;
	//y1 is equal for 3 pictures
	private int y1; 
	//y2 is used for text drawing 
	private int y2;
	
	
	private StaticLayout mTextLayout;

	ScreenManager screenManager;
		
	ProDrinkerEvent(ScreenManager screen, String text, int textSize){
		screenManager = screen;
			
		paint.setColor(Color.BLACK);
		paint.setAntiAlias(true);
		paint.setTypeface(HardPartLoader.tf);
		paint.setTextSize(Math.round(textSize * TEXT_SIZE_REDUCER));	
			
		startTime = System.currentTimeMillis();
		xBorder = Math.round(3*(screenManager.getScreenWidth()/4)) - HardPartLoader.drinkerImage2.getWidth();
		xShift = HardPartLoader.drinkerImage1.getWidth() + HardPartLoader.drinkerImage3.getWidth();
					
		this.text = text;
		
		x1 = 0 + HardPartLoader.drinkerImage3.getWidth();
		y1 = screenManager.getScreenHeight() - HardPartLoader.drinkerImage1.getHeight();
		y2 = screenManager.getScreenHeight() - (HardPartLoader.drinkerImage1.getHeight() / 2);
		x3 = xBorder;
		x2 = HardPartLoader.drinkerImage2.getWidth(); //step
		mTextLayout = new StaticLayout(text, paint, x3 - (HardPartLoader.drinkerImage1.getWidth() + HardPartLoader.drinkerImage3.getWidth()), Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
	}
	
	public long getShowingTime(){
		return SHOWING_TIME;
	}
	
	public String getText(){
		return text;
	}
	
	public int getId(){
		return id;
	}
	
	
	public long getStartTime(){
		return startTime;
	}
	
	//draw pro drinker - man with chat box
	public void drawEvent(Canvas canvas, int x, int y, int animationTime){	

		//picture of man and chat box opening
		canvas.drawBitmap(HardPartLoader.drinkerImage1, x1, y1, null);
				
		//fill the space between chat box opening and ending
		xShift = x1 + HardPartLoader.drinkerImage1.getWidth();
		while (xShift < xBorder){
			canvas.drawBitmap(HardPartLoader.drinkerImage2, xShift, y1, null);
			xShift += x2;
		}
		
		//picture of chat box ending
		canvas.drawBitmap(HardPartLoader.drinkerImage3, x3, y1, null);
		
		canvas.save();
		canvas.translate(HardPartLoader.drinkerImage1.getWidth() + HardPartLoader.drinkerImage3.getWidth(), y2);
		mTextLayout.draw(canvas);
		canvas.restore();
	}
	
}




class AchieveEvent implements Event{
	
	private long startTime = 0;
	
	private int id = 1;
	
	private static final int BIG_LETTER_ANIMATION_SPEED = 4;
	private static final float SMALL_TEXT_SIZE = 1.5f;
	private static final float   BIG_TEXT_SIZE = 1.8f;
	
	private Paint paint;
	private Paint paintStroke;
	private Paint paintAchiev;
	private String text;
	
	private int textSize;
	private int textSizeCurrent;
	private float xShift;
	
	private final String UNLOCKED_ACHIEVEMENT = "ACHIEVEMENT UNLOCKED";
	private final String UNLOCKED_SECRET_DRINK = "SECRET DRINK UNLOCKED";
	
	private String unlockedLable;
	
	private boolean isNew = false;
	
	public long innerTimer = 0;
	private int currentLetter = 0;
	private float sizeOfText = 0;

	
	AchieveEvent(String text, int achieveIndex, int textSize, boolean isNew, boolean isAchievement){
		this.text = text;
		this.isNew = isNew;
		
		if (isAchievement) unlockedLable = UNLOCKED_ACHIEVEMENT; else unlockedLable = UNLOCKED_SECRET_DRINK; 
		
		paint = new Paint();
		paintStroke = new Paint();

		paintStroke.setARGB(255, 255, 255, 255);	
		paintStroke.setStyle(Paint.Style.STROKE);
		paintStroke.setStrokeWidth(4);
		paintStroke.setAntiAlias(true);
		paintStroke.setTextAlign(Paint.Align.CENTER);	
		paintStroke.setTextSize(textSize);	
		paintStroke.setTypeface(HardPartLoader.tf);
		paint.setTypeface(HardPartLoader.tf);	
		paint.setTextSize(textSize);
		paint.setARGB(255, 0, 127, 125);
		paint.setTextAlign(Paint.Align.CENTER);
		paint.setAntiAlias(true);

		paintAchiev = paintStroke; //for counting space of label about unlocked achievement
		
		startTime = System.currentTimeMillis();
		innerTimer = startTime;
		currentLetter = 0;
		
		this.textSize = textSize;
	}
	
	
	public void drawEvent(Canvas canvas, int x, int y, int animationTime){
		textSizeCurrent = textSize;
		paint.setTextAlign(Paint.Align.CENTER);
		paintStroke.setTextAlign(Paint.Align.CENTER);
		
		//decrease text length if it's wider than screen width
		while (paintStroke.measureText(text) > canvas.getWidth()){
			--textSizeCurrent;
			paintStroke.setTextSize(textSizeCurrent);
			paint.setTextSize(textSizeCurrent);
		}

		canvas.drawText(text, x, y, paintStroke);
		canvas.drawText(text, x, y, paint);
		
		//shift for label about unlocked achievement
		paintAchiev.setTextSize(Math.round(textSize * SMALL_TEXT_SIZE));
		xShift = x - (paintStroke.measureText(text)/2) + ((paintStroke.measureText(text) - paintAchiev.measureText(unlockedLable))/2); 

		//draw text about unlocked new achievement
		if (isNew){			
			
			//checking for sizing up the next letter
			if (System.currentTimeMillis() - innerTimer > animationTime * BIG_LETTER_ANIMATION_SPEED){ //animation speed of big letter
				if (currentLetter < unlockedLable.length()) ++currentLetter;
				innerTimer = System.currentTimeMillis();
			}
			
			paint.setARGB(255, 0, 127, 85);
			paintStroke.setTextAlign(Paint.Align.LEFT);
			paint.setTextAlign(Paint.Align.LEFT);
			
			paint.setTextSize(Math.round(textSize * SMALL_TEXT_SIZE));
			paintStroke.setTextSize(Math.round(textSize * SMALL_TEXT_SIZE));
			
			//separate label about unlocked achievement: substring 1 + big letter + substring 2
			if (currentLetter < unlockedLable.length()){
				canvas.drawText(unlockedLable.substring(0, currentLetter), xShift, y - (textSize + Math.round(textSize/4)), paintStroke);
				canvas.drawText(unlockedLable.substring(0, currentLetter), xShift, y - (textSize + Math.round(textSize/4)), paint);
			
				sizeOfText = paintStroke.measureText(unlockedLable.substring(0, currentLetter));
			
				paint.setTextSize(Math.round(textSize * BIG_TEXT_SIZE));
				paintStroke.setTextSize(Math.round(textSize * BIG_TEXT_SIZE));
			
				canvas.drawText(unlockedLable.substring(currentLetter, currentLetter+1), xShift + sizeOfText, y - (textSize + Math.round(textSize/4)), paintStroke);
				canvas.drawText(unlockedLable.substring(currentLetter, currentLetter+1), xShift + sizeOfText, y - (textSize + Math.round(textSize/4)), paint);
			
				sizeOfText = sizeOfText + paintStroke.measureText(unlockedLable.substring(currentLetter, currentLetter+1));	
			
				paint.setTextSize(Math.round(textSize * SMALL_TEXT_SIZE));
				paintStroke.setTextSize(Math.round(textSize * SMALL_TEXT_SIZE));
					
				canvas.drawText(unlockedLable.substring(currentLetter+1, unlockedLable.length()), xShift + sizeOfText, y - (textSize + Math.round(textSize/4)), paintStroke);
				canvas.drawText(unlockedLable.substring(currentLetter+1, unlockedLable.length()), xShift + sizeOfText, y - (textSize + Math.round(textSize/4)), paint);
			}
				else{
					canvas.drawText(unlockedLable, xShift, y - (textSize + Math.round(textSize/4)), paintStroke);
					canvas.drawText(unlockedLable, xShift, y - (textSize + Math.round(textSize/4)), paint);
				}
		}
	}	
	

	
	public long getShowingTime(){
		return showingTime;
	}
	
	public String getText(){
		return text;
	}
	
	
	public int getId(){
		return id;
	}
	
	public long getStartTime(){
		return startTime;
	}
}	
	
