package com.alexgolikov.getloaded;

import java.util.ArrayList;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;




public class HardPartLoader {

	private static ArrayList<Drink> allDrinks = new ArrayList<Drink>();
	private static ArrayList<Drink> allSecretDrinks = new ArrayList<Drink>();
	
	public static Typeface tf;
	
	public static Bitmap[] absintheImage = new Bitmap[1];
	public static Bitmap[] b_52Image = new Bitmap[6];
	public static Bitmap[] bloodmaryImage = new Bitmap[5];
	public static Bitmap[] champagneImage = new Bitmap[5];
	public static Bitmap[] longislandImage = new Bitmap[5];
	public static Bitmap[] margaritaImage = new Bitmap[5];
	public static Bitmap[] mojitoImage = new Bitmap[5];
	public static Bitmap[] vodkaImage = new Bitmap[1];
	public static Bitmap[] tequilaImage = new Bitmap[1];
	public static Bitmap[] beerImage = new Bitmap[5];
	public static Bitmap[] unknownImage = new Bitmap[5];
	public static Bitmap[] cognacImage = new Bitmap[6];
	public static Bitmap[] wineImage = new Bitmap[6];
	public static Bitmap[] points100Image = new Bitmap[6];
	public static Bitmap[] points250Image = new Bitmap[6];
	public static Bitmap[] points500Image = new Bitmap[6];
	public static Bitmap[] secretDrink1 = new Bitmap[1];
	public static Bitmap[] secretDrink1_blank = new Bitmap[1];
	
	public static Bitmap background_0;
	public static Bitmap background_1;
	public static Bitmap background_2;
	public static Bitmap background_how_to;
	
	public static Bitmap drinkerImage1;
	public static Bitmap drinkerImage2;
	public static Bitmap drinkerImage3;
	

	
	public HardPartLoader(Context context) {
	
		Resources mRes = context.getResources();
		loadAllBitmaps(mRes);
		
		createDrinksMassive();
		
		tf = Typeface.createFromAsset(context.getAssets(), "fonts/KOMIKAX_.ttf");
	}
	

	
	public static ArrayList<Drink> getDrinksMassive(){
		return allDrinks;
	}
	
	
	public static ArrayList<Drink> getSecretDrinksMassive(){
		return allSecretDrinks;
	}
	
		

	
	private void loadAllBitmaps(Resources mRes){
		
		background_0 = BitmapFactory.decodeResource(mRes, R.drawable.background_0);
		background_1 = BitmapFactory.decodeResource(mRes, R.drawable.background_1);
		background_2 = BitmapFactory.decodeResource(mRes, R.drawable.background_2);
		background_how_to = BitmapFactory.decodeResource(mRes, R.drawable.background_how_to);
		
		drinkerImage1 = BitmapFactory.decodeResource(mRes, R.drawable.prodrinker1);	
		drinkerImage2 = BitmapFactory.decodeResource(mRes, R.drawable.prodrinker2);
		drinkerImage3 = BitmapFactory.decodeResource(mRes, R.drawable.prodrinker3);
		
		absintheImage[0] = BitmapFactory.decodeResource(mRes, R.drawable.absinthe_glass);
				
		b_52Image[0] = BitmapFactory.decodeResource(mRes, R.drawable.b_52_glass_1);
		b_52Image[1] = BitmapFactory.decodeResource(mRes, R.drawable.b_52_glass_2);
		b_52Image[2] = BitmapFactory.decodeResource(mRes, R.drawable.b_52_glass_3);
		b_52Image[3] = BitmapFactory.decodeResource(mRes, R.drawable.b_52_glass_4);
		b_52Image[4] = BitmapFactory.decodeResource(mRes, R.drawable.b_52_glass_5);
		b_52Image[5] = BitmapFactory.decodeResource(mRes, R.drawable.b_52_glass_6);
		
		bloodmaryImage[0] = BitmapFactory.decodeResource(mRes, R.drawable.blood_mary_glass_1);
		bloodmaryImage[1] = BitmapFactory.decodeResource(mRes, R.drawable.blood_mary_glass_2);
		bloodmaryImage[2] = BitmapFactory.decodeResource(mRes, R.drawable.blood_mary_glass_3);
		bloodmaryImage[3] = BitmapFactory.decodeResource(mRes, R.drawable.blood_mary_glass_4);
		bloodmaryImage[4] = BitmapFactory.decodeResource(mRes, R.drawable.blood_mary_glass_5);
		
		champagneImage[0] = BitmapFactory.decodeResource(mRes, R.drawable.champagne_glass_1);
		champagneImage[1] = BitmapFactory.decodeResource(mRes, R.drawable.champagne_glass_2);
		champagneImage[2] = BitmapFactory.decodeResource(mRes, R.drawable.champagne_glass_3);
		champagneImage[3] = BitmapFactory.decodeResource(mRes, R.drawable.champagne_glass_4);
		champagneImage[4] = BitmapFactory.decodeResource(mRes, R.drawable.champagne_glass_5);
		
		longislandImage[0] = BitmapFactory.decodeResource(mRes, R.drawable.long_island_glass_1);
		longislandImage[1] = BitmapFactory.decodeResource(mRes, R.drawable.long_island_glass_2);
		longislandImage[2] = BitmapFactory.decodeResource(mRes, R.drawable.long_island_glass_3);
		longislandImage[3] = BitmapFactory.decodeResource(mRes, R.drawable.long_island_glass_4);
		longislandImage[4] = BitmapFactory.decodeResource(mRes, R.drawable.long_island_glass_5);
		
		margaritaImage[0] = BitmapFactory.decodeResource(mRes, R.drawable.margarita_glass_1);
		margaritaImage[1] = BitmapFactory.decodeResource(mRes, R.drawable.margarita_glass_2);
		margaritaImage[2] = BitmapFactory.decodeResource(mRes, R.drawable.margarita_glass_3);
		margaritaImage[3] = BitmapFactory.decodeResource(mRes, R.drawable.margarita_glass_4);
		margaritaImage[4] = BitmapFactory.decodeResource(mRes, R.drawable.margarita_glass_5);
		
		mojitoImage[0] = BitmapFactory.decodeResource(mRes, R.drawable.mojito_glass_1);
		mojitoImage[1] = BitmapFactory.decodeResource(mRes, R.drawable.mojito_glass_2);
		mojitoImage[2] = BitmapFactory.decodeResource(mRes, R.drawable.mojito_glass_3);
		mojitoImage[3] = BitmapFactory.decodeResource(mRes, R.drawable.mojito_glass_4);
		mojitoImage[4] = BitmapFactory.decodeResource(mRes, R.drawable.mojito_glass_5);
		
		tequilaImage[0] = BitmapFactory.decodeResource(mRes, R.drawable.tequila_glass_1);
		
		unknownImage[0] = BitmapFactory.decodeResource(mRes, R.drawable.unknown_glass_1);
		unknownImage[1] = BitmapFactory.decodeResource(mRes, R.drawable.unknown_glass_2);
		unknownImage[2] = BitmapFactory.decodeResource(mRes, R.drawable.unknown_glass_3);
		unknownImage[3] = BitmapFactory.decodeResource(mRes, R.drawable.unknown_glass_4);
		unknownImage[4] = BitmapFactory.decodeResource(mRes, R.drawable.unknown_glass_5);
		
		wineImage[0] = BitmapFactory.decodeResource(mRes, R.drawable.wine_glass_1);
		wineImage[1] = BitmapFactory.decodeResource(mRes, R.drawable.wine_glass_2);
		wineImage[2] = BitmapFactory.decodeResource(mRes, R.drawable.wine_glass_3);
		wineImage[3] = BitmapFactory.decodeResource(mRes, R.drawable.wine_glass_4);
		wineImage[4] = BitmapFactory.decodeResource(mRes, R.drawable.wine_glass_5);
		wineImage[5] = BitmapFactory.decodeResource(mRes, R.drawable.wine_glass_6);
			
		vodkaImage[0] = BitmapFactory.decodeResource(mRes, R.drawable.vodka_glass_1);
		
		beerImage[0]  = BitmapFactory.decodeResource(mRes, R.drawable.drink_beer_1);
		beerImage[1]  = BitmapFactory.decodeResource(mRes, R.drawable.drink_beer_2);
		beerImage[2]  = BitmapFactory.decodeResource(mRes, R.drawable.drink_beer_3);
		beerImage[3]  = BitmapFactory.decodeResource(mRes, R.drawable.drink_beer_4);
		beerImage[4]  = BitmapFactory.decodeResource(mRes, R.drawable.drink_beer_5);
		
		cognacImage[0]  = BitmapFactory.decodeResource(mRes, R.drawable.drink_cognac_1);
		cognacImage[1]  = BitmapFactory.decodeResource(mRes, R.drawable.drink_cognac_2);
		cognacImage[2]  = BitmapFactory.decodeResource(mRes, R.drawable.drink_cognac_3);
		cognacImage[3]  = BitmapFactory.decodeResource(mRes, R.drawable.drink_cognac_4);
		cognacImage[4]  = BitmapFactory.decodeResource(mRes, R.drawable.drink_cognac_5);
		cognacImage[5]  = BitmapFactory.decodeResource(mRes, R.drawable.drink_cognac_6);
		
		points100Image[0] = BitmapFactory.decodeResource(mRes, R.drawable.points100_1);
		points100Image[1] = BitmapFactory.decodeResource(mRes, R.drawable.points100_2);
		points100Image[2] = BitmapFactory.decodeResource(mRes, R.drawable.points100_3);
		points100Image[3] = BitmapFactory.decodeResource(mRes, R.drawable.points100_4);
		points100Image[4] = BitmapFactory.decodeResource(mRes, R.drawable.points100_5);
		points100Image[5] = BitmapFactory.decodeResource(mRes, R.drawable.points100_6);
		
		points250Image[0] = BitmapFactory.decodeResource(mRes, R.drawable.points250_1);
		points250Image[1] = BitmapFactory.decodeResource(mRes, R.drawable.points250_2);
		points250Image[2] = BitmapFactory.decodeResource(mRes, R.drawable.points250_3);
		points250Image[3] = BitmapFactory.decodeResource(mRes, R.drawable.points250_4);
		points250Image[4] = BitmapFactory.decodeResource(mRes, R.drawable.points250_5);
		points250Image[5] = BitmapFactory.decodeResource(mRes, R.drawable.points250_6);
		
		points500Image[0] = BitmapFactory.decodeResource(mRes, R.drawable.points500_1);
		points500Image[1] = BitmapFactory.decodeResource(mRes, R.drawable.points500_2);
		points500Image[2] = BitmapFactory.decodeResource(mRes, R.drawable.points500_3);
		points500Image[3] = BitmapFactory.decodeResource(mRes, R.drawable.points500_4);
		points500Image[4] = BitmapFactory.decodeResource(mRes, R.drawable.points500_5);
		points500Image[5] = BitmapFactory.decodeResource(mRes, R.drawable.points500_6);
		
		secretDrink1[0] = BitmapFactory.decodeResource(mRes, R.drawable.secret_drink_1);
		secretDrink1_blank[0] = BitmapFactory.decodeResource(mRes, R.drawable.secret_drink_blank);
		
	}
	
	
	
	
	
	private void createDrinksMassive(){
		
		allDrinks.clear();
		allDrinks.add(new BeerDrink(0, 0, beerImage));
		allDrinks.add(new MojitoDrink(0, 0, mojitoImage));
		allDrinks.add(new BloodMaryDrink(0, 0, bloodmaryImage));
		allDrinks.add(new WineDrink(0, 0, wineImage));
		allDrinks.add(new ChampagneDrink(0, 0, champagneImage));
		allDrinks.add(new LongIslandDrink(0, 0, longislandImage));
		allDrinks.add(new B52Drink(0, 0, b_52Image));
		allDrinks.add(new MargaritaDrink(0, 0, margaritaImage));
		allDrinks.add(new CognacDrink(0, 0, cognacImage));
		allDrinks.add(new TequilaDrink(0, 0, tequilaImage));
		allDrinks.add(new VodkaDrink(0, 0, vodkaImage));
		allDrinks.add(new AbsintheDrink(0, 0, absintheImage));
		allDrinks.add(new UnknownDrink(0, 0, unknownImage));
		allDrinks.add(new Points100Drink(0, 0, points100Image));
		allDrinks.add(new Points250Drink(0, 0, points250Image));
		allDrinks.add(new Points500Drink(0, 0, points500Image));
		allDrinks.add(new ButterBeer(0, 0, secretDrink1, secretDrink1_blank));
		
		allSecretDrinks.clear();
		allSecretDrinks.add(new ButterBeer(0, 0, secretDrink1, secretDrink1_blank));
		
	}
	

	
}
