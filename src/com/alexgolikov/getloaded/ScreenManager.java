package com.alexgolikov.getloaded;

import android.content.Context;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.WindowManager;



public class ScreenManager {
	
	private int SCREEN_WIDTH = 0;
	private int SCREEN_HEIGHT = 0;
	
	//private int SCREEN_WIDTH_DP = 0;
	//private int SCREEN_HEIGHT_DP = 0;
	
	private int ANIMATION_TIME = 15;
	
	private int MAX_FPS = 30;
	private float FPS = 0;
	private int FRAME_SKIPPED = 20;
	 
	private float DRINK_SPEED = 0;
	private int DRINK_SPACE = 0;  //in pixels
	private int DRINK_LEVEL = 0;
	
	private int LINE_OF_DRINKING = 0;

	private int FRAME_CHANGE_TIME = 20; // in msec
	
	private int TEXT_SIZE;

	public static Typeface tf;
	
	public ScreenManager(Context context){
	
		DisplayMetrics metrics = new DisplayMetrics();
		WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		windowManager.getDefaultDisplay().getMetrics(metrics);
		//metrics = context.getResources().getDisplayMetrics();
		
		FPS = 1000 / MAX_FPS; //1000 ms = 1 sec
		
		SCREEN_WIDTH = metrics.widthPixels;
		SCREEN_HEIGHT = metrics.heightPixels;

		//SCREEN_WIDTH_DP = (int)(SCREEN_WIDTH /metrics.density);
		//SCREEN_HEIGHT_DP = (int)(SCREEN_HEIGHT /metrics.density);
		
		//DRINK_SPEED = Math.round((SCREEN_WIDTH / 6) / FPS);
		DRINK_SPEED = 10.0f;//SCREEN_WIDTH / 600; //in msec * 10
		DRINK_LEVEL = Math.round(SCREEN_HEIGHT / 5);
		
		DRINK_SPACE = SCREEN_WIDTH / 5; //space between drinks (how many drinks on the screen in one time)
		
		//border of drinking holding drink
		LINE_OF_DRINKING = Math.round(SCREEN_HEIGHT / 20);
		
		TEXT_SIZE = Math.round(SCREEN_HEIGHT / 11);
		
		tf = HardPartLoader.tf;
		//tf = Typeface.createFromAsset(context.getAssets(), "fonts/KOMIKAX_.ttf");
	}
	
	
	public Typeface getTypeFace(){
		return tf;
	}
	
	
	public int getScreenWidth(){
		return SCREEN_WIDTH;
	}
	
	
	public int getScreenHeight(){
		return SCREEN_HEIGHT;
	}
	
	
	public int getDrinkSpace(){
		return DRINK_SPACE;
	}
		

	public int getTextSize(){
		return TEXT_SIZE;
	}

	
	public float getDrinkSpeed(){
		return DRINK_SPEED;
	}
	
	
	public void setDrinkSpace(int space){
		DRINK_SPACE = space;
	}

	
	public int getDrinkLevel(){
		return DRINK_LEVEL;
	}
	
	
	public int getLineOfDrinking(){
		return LINE_OF_DRINKING;
	}
	
	
	public int getAnimationTime(){
		return ANIMATION_TIME;
	}
	
	public int getMaxFPS(){
		return MAX_FPS;
	}
	
	public int getFrameSkipped(){
		return FRAME_SKIPPED;
	}
	
	public float getFPS(){
		return FPS;
	}
	
	public int getFrameChangeTime(){
		return FRAME_CHANGE_TIME;
	}
}
