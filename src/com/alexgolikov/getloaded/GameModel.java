package com.alexgolikov.getloaded;

import java.util.ArrayList;

public class GameModel {
	
	//the massive of the last 5 drinks for intoxication calculation
	private static ArrayList<Drink> drinksMass = new ArrayList<Drink>();  

	//amount of pre-created drinks in the object pool  
	public static final int DRINKS_COUNT_IN_POOL = 100; 
	
	//amount of steps for shuffling the object pool
	public static final int SHUFFLING_POOL_STEPS = 50;
	
	//maximum drinks count for checking intoxication and achievements
	public static final int DRINKS_COUNT_FOR_ACHIEVEMENT_CHECK = 5;
	
	//points of penalty for stopping drinking
	private static final int POINTS_PENALTY = 10;
	private static final long POINTS_PENALTY_BORDER = 3000; //after this time of pausing in drinking, points start to decrease
	private static final long POINTS_PENALTY_TIME = 800; //points are decreased POINTS_PENALTY every this time
	
	//power of increasing points for drinking unknown drink
	public static final int POWER_FOR_UNKNOWN = 3;  
	
	//points of increasing speed of drinks sliding
	private static final float SPEED_INCREASING = 0.5f;
	private static final int SPEED_INCREASING_TIME = 30; //seconds
	
    public static final int STATE_START = 0;
    public static final int STATE_PLAY = 1;
    public static final int STATE_LOSE = 2;
    public static final int STATE_PAUSE = 3;
    
    private static int current_state;
	
    private static boolean howToScreenShowed = false;
	
	static void addDrinkToMass(Drink drink){	
		
		drinksMass.add(0, drink);		
		if (drinksMass.size() > DRINKS_COUNT_FOR_ACHIEVEMENT_CHECK) drinksMass.remove(drinksMass.size()-1);
		
	}
	
	
	public static void setGameState(int state){
		current_state = state;
	}
	
	
	public static boolean isHowToScreenShowed(){
		return howToScreenShowed;
	}
	
	
	public static void setHowToScreenShowed(boolean state){
		howToScreenShowed = state;
	}
	
	
	public static int getGameState(){
		return current_state;
	}
	
	
	public static ArrayList<Drink> getDrinksMass(){
		return drinksMass;
	}

	
	static float calculateStrength(Drink drink){
		
		float strength = drink.getStrength();
		float sum = 0;
		
		addDrinkToMass(drink);
		
		//the main logic of the game: calculate points in order to strength of last five drinks
		for (int i = 0; i < drinksMass.size()-1; i++){
			if (drinksMass.get(i).getStrength() < drinksMass.get(i+1).getStrength()) sum += ((drinksMass.get(i+1).getStrength() - drinksMass.get(i).getStrength()) + (drinksMass.size() - i));
		}
		
		if (sum == 0) strength = 0; else strength = strength * ((sum * 0.1f) + 1);
		
		return strength; 
	}
	
	
	public static void clearDrinkMass(){
		drinksMass.clear();
	}
	
	
	public static int getPointsPenalty(){
		return POINTS_PENALTY;
	}
	
	
	
	public static int getAmountOfDrinksInPool(){
		return DRINKS_COUNT_IN_POOL;
	}
	
	
	public static long getPointsPenaltyTime(){
		return POINTS_PENALTY_TIME;
	}
	
	
	public static long getPointsPenaltyBorder(){
		return POINTS_PENALTY_BORDER;
	}
	
	
	public static float getSpeedIncreasing(){
		return SPEED_INCREASING;
	}
	
	
	public static int getSpeedIncreasingTime(){
		return SPEED_INCREASING_TIME;
	}
	
}
