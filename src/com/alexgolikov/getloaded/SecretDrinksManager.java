package com.alexgolikov.getloaded;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import android.test.IsolatedContext;
import android.util.Log;

public class SecretDrinksManager {

	//points to get the drink
	public static final int SECRET_DRINK_1_BORDER = 900;
	
	private static Set<Integer> secretDrinksSet = new HashSet<Integer>();
	private static ArrayList<SecretDrinkWrap> secretDrinks = new ArrayList<SecretDrinkWrap>();
	
	
	
	public static boolean checkSecretDrink(Integer id){
		if (secretDrinksSet.contains(id)) return true;
			else return false;
	}
	
	
	public static int getSecretDrinksAmount(){
		return secretDrinksSet.size();
	}
	
	
	public static void setDrinksList(Set setOfDrinks){
		secretDrinksSet.clear();
		secretDrinksSet = setOfDrinks;	
		Log.v("------->", "Drinks in DB: " + Integer.toString(secretDrinksSet.size()));
		System.out.println("------------> Drinks in DB: " + Integer.toString(secretDrinksSet.size()));
		
		
		secretDrinks.clear();
		secretDrinks.add(new SecretDrinkWrap(new ButterBeer(0, 0, HardPartLoader.secretDrink1, HardPartLoader.secretDrink1_blank), SECRET_DRINK_1_BORDER, checkSecretDrink(Drink.SECRET_DRINK_1)));
	}
	
	
	public static int checkPoints(int points){
		
		int i = 0;
		Log.v("------->", "Checking points");
		System.out.println("---------------> Checking points");
		while (i < secretDrinks.size() && secretDrinks.get(i).getBorder() < points){
			if (!secretDrinks.get(i).isChecked()){
				Log.v("------->", "ENOUGHT POINTS! We are ready to unlock new secret drink");
				System.out.println("------------> ENOUGHT POINTS! We are ready to unlock new secret drink");
				secretDrinks.get(i).setChecked(true);
				return secretDrinks.get(i).getDrinkId();
			}
			i++;
		}
		
		return -1;
		
	}
	
	
	
}



class SecretDrinkWrap{
	
	private Drink drink;
	private int border;
	private boolean checked;
	
	
	SecretDrinkWrap(Drink drink, int border, boolean checked){
		
		this.drink = drink;
		this.border = border;
		this.checked = checked;
		
	}
	
	
	public int getBorder(){
		return border;
	}
	
	public boolean isChecked(){
		return checked;
	}
	
	
	public int getDrinkId(){
		return this.drink.getId();
	}
	
	public void setChecked(boolean setter){
		this.checked = setter;
	}
	
	
}
