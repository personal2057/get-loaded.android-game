package com.alexgolikov.getloaded;



public class GameTimer{
	
	private long now_time = 0;
	private long penalty_time = 0;
	private long change_frame_time = 0;
	
	//variables for clock in game
	private long elapsed_clock_time = 0;
	private long prev_timer_time = 0;
	
	//variables for changing frames
	private long elapsed_frame_change_time = 0;
	private long prev_frame_change_time = 0;
	
	ScreenManager screenManager;
	EventManager eventManager;
	private Player player;
	
	//variables for clock in game
	private int minutes = 0;
	private int seconds = 0;
	
	private boolean penaltyReady = false;
	private boolean frameChangeReady = false;
	private boolean speedIncreasingReady = false;
	
	GameTimer(ScreenManager screenManager, EventManager eventManager, Player player){
		
		this.player = player;
		now_time = System.currentTimeMillis();
		prev_timer_time = now_time;
		penalty_time = now_time;
		change_frame_time = now_time;
		
		this.screenManager = screenManager;
		this.eventManager = eventManager;
		
		minutes = 0;
		seconds = 0;
	}
	
	
	public void go(){
		
		//get real time
		now_time = System.currentTimeMillis(); 	
        elapsed_clock_time = now_time - prev_timer_time;
        elapsed_frame_change_time = now_time - prev_frame_change_time;

    	//changing frames in bitmaps
    	if (elapsed_frame_change_time > screenManager.getFrameChangeTime()){
    		frameChangeReady = true;
    		prev_frame_change_time = now_time;
    	}
    	
        //if pasts one second
        if (elapsed_clock_time > 1000){
        	
        	if (seconds == 59){
        			seconds = 0;
        			minutes++;
        	}
        		else seconds++;
        	
        	
        	//checking for achievements
        	eventManager.checkAchievmentEvent(minutes, seconds, player);
        	eventManager.checkProDrinkerEvent(minutes, seconds, player);
        	
        	//increasing sliding speed of the drinks
        	if (seconds % GameModel.getSpeedIncreasingTime() == 0) speedIncreasingReady = true;
        	
        	prev_timer_time = now_time;
        }
             		
        
		//add penalty for stopping drinking
		if ((now_time - DrinkManager.getDrinkingTime()) > GameModel.getPointsPenaltyBorder()){
			
			if ((now_time - penalty_time) > GameModel.getPointsPenaltyTime()){
				//penaltyReady = true;
				player.setPoints(GameModel.getPointsPenalty() * (-1));
				penalty_time = System.currentTimeMillis();
			}
		}
        
	}
	
	public int getSeconds(){
		return seconds;
	}
	
	public int getMinutes(){
		return minutes;
	}
	

	
	public boolean isSpeedIncreasingReady(){
		return speedIncreasingReady;
	}
		

	public boolean isFrameChangeReady(){
		return frameChangeReady;
	}
	
	
	public boolean isPenaltyReady(){
		return penaltyReady;
	}
	
	
	public void setFrameChangeReady(boolean setter){
		frameChangeReady = setter;
	}
	
	
	public void setPenaltyReady(boolean setter){
		penaltyReady = setter;
	}

	
	public void setSpeedIncreasingReady(boolean setter){
		speedIncreasingReady = setter;
	}

}

