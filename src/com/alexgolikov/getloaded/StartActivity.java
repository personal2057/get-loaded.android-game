package com.alexgolikov.getloaded;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.app.Activity;




public class StartActivity extends Activity {

	private StartView mStartView;
	
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);   
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        setContentView(R.layout.activity_start);
        
        HardPartLoader hardPartLoader = new HardPartLoader(this);
        
        mStartView = new StartView(this, this);    
        setContentView(mStartView);
        
    }

    

    
    @Override
    protected void onPause() {
        super.onPause();
        
        mStartView.onPauseSurfaceView();
    }

    
    
    
    @Override
    protected void onResume() {
        super.onResume();
        
        mStartView.onResumeSurfaceView();
    }
    
    
    
    
    @Override
    public void onStop(){
    	
        super.onStop();
    }
    
   
    
    @Override
    public void onDestroy(){
        super.onDestroy();

    }
    
}


