package com.alexgolikov.getloaded;


import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.app.Activity;
import android.content.Context;




public class GameView extends SurfaceView implements SurfaceHolder.Callback {
	
	
	private GameThread thread;
	DrinkManager drinkManager;
	EventManager eventManager;
	SurfaceHolder holder;
	Context context;
	Activity activity;
	
	private boolean isHowToScreenShowed = false;
	
    public GameView(Context context, Activity activity) {
        super(context);
        
        holder = getHolder();
        holder.addCallback(this);
        this.context = context;
        this.activity = activity;
        
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);
    }  
	
    
    
    class GameThread extends Thread{

        private Bitmap mBackgroundImageFar; 
        private Bitmap mBackgroundImageFarEnd; 
        private Bitmap mBackgroundImageHowTo; 
        
        private SurfaceHolder mSurfaceHolder;
        
        public GameTimer mTimer;  
        public ScreenManager screenManager;
        public Player player;
        public GUI mainGUI;
        
        private boolean runFlag = false;
        
        Paint paint = new Paint();

        
        public GameThread(SurfaceHolder surfaceHolder, Context context, Handler handler){
            this.mSurfaceHolder = surfaceHolder;
                        
            screenManager = new ScreenManager(context);
            eventManager = new EventManager(context, screenManager);
            player = new Player(eventManager);
            player.clearDrankMass();
            mTimer = new GameTimer(screenManager, eventManager, player);
            mainGUI = new GUI(screenManager, context, mTimer, player);
            
            drinkManager = new DrinkManager(context, screenManager, player, mTimer, mainGUI);
            drinkManager.addDrink();
            
            mBackgroundImageFar = HardPartLoader.background_1;
            mBackgroundImageFar = Bitmap.createScaledBitmap(mBackgroundImageFar, screenManager.getScreenWidth(), screenManager.getScreenHeight(), true);
            mBackgroundImageFarEnd = mBackgroundImageFar;
            mBackgroundImageFarEnd = HardPartLoader.background_2;
            mBackgroundImageFarEnd = Bitmap.createScaledBitmap(mBackgroundImageFarEnd, screenManager.getScreenWidth(), screenManager.getScreenHeight(), true);
            mBackgroundImageHowTo = HardPartLoader.background_how_to;
            mBackgroundImageHowTo = Bitmap.createScaledBitmap(mBackgroundImageHowTo, screenManager.getScreenWidth(), screenManager.getScreenHeight(), true);
           
            GameModel.clearDrinkMass();
            eventManager.clearEventMassives();

            paint.setColor(Color.WHITE);
            paint.setTextSize(20);
            isHowToScreenShowed = GameModel.isHowToScreenShowed();
        }

        
                
        
        public void setRunning(boolean run) {
            runFlag = run;
            
            if (run){
            	player.setCanDrink(run);
            }
        }

   
        
        
        @Override
        public void run() {         
            
            long beginTime;   
            long timeDiff;     
            
            int sleepTimeTEST = 0; //just for test
            int sleepTime = 0;     
            //int framesSkipped;
            
            Canvas c = null;
                        
            while (runFlag) {          	
            	
            	
            	beginTime = System.currentTimeMillis();
            	
            	if (mSurfaceHolder.getSurface().isValid()){
            	try{
            		c = mSurfaceHolder.lockCanvas(null);
            		synchronized (mSurfaceHolder) {
            			            
            			if (!isHowToScreenShowed){
        					c.drawBitmap(mBackgroundImageHowTo, 0, 0, null);
            			} else
            			if (player.isCanDrink()){
            				
            				c.drawBitmap(mBackgroundImageFar, 0, 0, null);
            				drinkManager.drawDrinks(c);
            				eventManager.drawEvents(c);               		
            				mainGUI.drawGUI(c);
            				
            				//for test 
            				if (sleepTime <= 0) sleepTimeTEST = sleepTime;
            				c.drawText(Integer.toString(sleepTimeTEST), 20, screenManager.getScreenHeight()/2, paint);
            				c.drawText(Double.toString(screenManager.getFPS()), 20, screenManager.getScreenHeight()/2 + 50, paint);
            				

            			}
            				else if (!player.isCanDrink()){
            					c.drawBitmap(mBackgroundImageFarEnd, 0, 0, null);
            					GUI.finalScreen.drawFinalScreen(c);
            				}
            		}
            	}            		
                finally {
                	if (c != null) {
                		mSurfaceHolder.unlockCanvasAndPost(c);
                    }
                }
            	
            	if (player.isCanDrink() && isHowToScreenShowed){
            		
            		//drinkManager.makeStep(screenManager.getDrinkSpeed());
            		
            		mTimer.go();
    		
            		//increasing sliding speed of the drinks
            		if (mTimer.isSpeedIncreasingReady()){
            			drinkManager.increaseSpeed(GameModel.getSpeedIncreasing());
            			mTimer.setSpeedIncreasingReady(false);
            		}  
    		
            		//change bitmaps frames
            		if (mTimer.isFrameChangeReady()){
            			drinkManager.changeFrames();
            			mTimer.setFrameChangeReady(false);
            		} 
    		
				
            	}
            	

				timeDiff = System.currentTimeMillis() - beginTime;			
				
				sleepTime = (int)(screenManager.getFPS() - timeDiff);
				drinkManager.makeStep(sleepTime);
				//drinkManager.makeStep(screenManager.getDrinkSpeed() * timeDiff);
				
				if (sleepTime > 0){
					try{
						Thread.sleep(sleepTime);
					} catch (InterruptedException e) {}

				}else
					{
					
					if (isHowToScreenShowed && player.isCanDrink()){

						int per = (int)(sleepTime * (-1) / screenManager.getFPS());
						if (per > 0)
							drinkManager.makeStep(screenManager.getDrinkSpeed() * per);
					
					}
					
					}
          	           	
            }
            }
            
            /*
            if (!player.isCanDrink()){
            	
                if (mSurfaceHolder.getSurface().isValid()){
                	try{
                		c = mSurfaceHolder.lockCanvas(null);
                		synchronized (mSurfaceHolder) {
                			c.drawBitmap(mBackgroundImageFar, 0, 0, null);
                			mainGUI.showFinalScreen(c);
                		}
                	}
                	finally{
                		if (c != null) mSurfaceHolder.unlockCanvasAndPost(c);
                	}
                }
            }
            */
        }}


    

    public boolean onTouchEvent(MotionEvent event) {
    	
    	if (!GameModel.isHowToScreenShowed()){
			switch (event.getAction()){
				case MotionEvent.ACTION_UP:{
					isHowToScreenShowed = true;
					GameModel.setHowToScreenShowed(isHowToScreenShowed);
				}
				break;
			}
		} else
			
    	if (GameModel.getGameState() == GameModel.STATE_PLAY){
    		switch (event.getAction()){
    			case MotionEvent.ACTION_DOWN: drinkManager.checkClick(event.getX(0), event.getY(0));
    			 	break;
    		
    			case MotionEvent.ACTION_MOVE: drinkManager.checkHold(event.getX(0), event.getY(0));
    			 	break;
    			 
    			case MotionEvent.ACTION_UP:   drinkManager.checkReturn(event.getX(), event.getY());
    			 	break;
    		}
    	} else
    		if (GameModel.getGameState() == GameModel.STATE_LOSE){
    			switch (event.getAction()){
        			case MotionEvent.ACTION_DOWN: GUI.finalScreen.setClick(event.getX(0), event.getY(0));		
       			 		break;
       			 
        			case MotionEvent.ACTION_UP: if (GUI.finalScreen.getLine() == 3){
        				onPauseSurfaceView();
    					activity.finish();
        			}
        				break;
    			}
    		};
    			
    	
    	
    	return true;
    }
    
    
    
    public GameThread getThread() {
        return thread;
    }
    
    
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {	
    }
 
    public void surfaceCreated(SurfaceHolder holder) {
 
    }
    
    public void surfaceDestroyed(SurfaceHolder holder) {

    }
    
    
    
    public void onResumeSurfaceView(){
       	thread = new GameThread(holder, getContext(), new Handler());
       	GameModel.setGameState(GameModel.STATE_PLAY);
       	isHowToScreenShowed = GameModel.isHowToScreenShowed();
        thread.setRunning(true);
        thread.start();
    }
    
    
    public void onPauseSurfaceView(){
        boolean retry = true;
        thread.setRunning(false);
        
        while (retry) {
            try {          	
                thread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }

    }
    	   
    
}







