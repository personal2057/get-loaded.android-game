package com.alexgolikov.getloaded;

import java.util.ArrayList;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.Shader.TileMode;




public class GUI {
	
	TimerLabel timerLabel;
	ScoreLabel scoreLabel;
	LoadedLabel loadedLabel;
	public static LastDrinkLabel lastDrinkLabel;
	public static FinalScreen finalScreen;
	
	Player player;
	//public static Typeface tf;
	GameTimer gameTimer;
	Context context;
	ScreenManager screenManager;	
	MySQLiteHelper scoreSQLBase;
	private Menu finalScreenMenu;
	
	//variables for painting circle around drink and it's animation
	private Paint circlePaint = new Paint();
	private int circlePaintAlpha = 100;
	private int circlePaintAlphaStep = 5;
	private final int CIRCLE_PAINT_RED = 255;
	private final int CIRCLE_PAINT_GREEN = 255;
	private final int CIRCLE_PAINT_BLUE = 255;
	private final int CIRCLE_PAINT_HIGH_BORDER = 220;
	private final int CIRCLE_PAINT_LOW_BORDER = 25;
	private final int CIRCLE_PAINT_STROKE_WIDTH = 8;
	private final float CIRCLE_PAINT_RADIUS;
	
	//massive of drank labels
	ArrayList<DrankLabel> drank_label_mass = new ArrayList<DrankLabel>();
	
	public GUI(ScreenManager screenManager, Context context, GameTimer gameTimer, Player player){
		
		//tf = Typeface.createFromAsset(context.getAssets(), "fonts/KOMIKAX_.ttf");
		scoreSQLBase = new MySQLiteHelper(context);
		
		this.gameTimer = gameTimer;
		this.player = player;
		this.screenManager = screenManager;
		this.context = context;
		
		finalScreen = new FinalScreen();
		finalScreenMenu = new Menu(context, screenManager, context.getResources().getString(R.string.final_screen_title));
		
		timerLabel = new TimerLabel(screenManager.getScreenWidth(), screenManager.getScreenHeight(), HardPartLoader.tf, screenManager.getTextSize());
		scoreLabel = new ScoreLabel(screenManager.getScreenWidth(), screenManager.getScreenHeight(), HardPartLoader.tf, screenManager.getTextSize());
		loadedLabel = new LoadedLabel(screenManager.getScreenWidth(), screenManager.getScreenHeight(), HardPartLoader.tf, screenManager.getTextSize(), player.getIntoxicationLimit());
		lastDrinkLabel = new LastDrinkLabel();
		
		circlePaint.setStyle(Paint.Style.STROKE);
		circlePaint.setARGB(circlePaintAlpha, CIRCLE_PAINT_RED, CIRCLE_PAINT_GREEN, CIRCLE_PAINT_BLUE);
		circlePaint.setStrokeWidth(CIRCLE_PAINT_STROKE_WIDTH);
		circlePaint.setAntiAlias(true);
		
		CIRCLE_PAINT_RADIUS = HardPartLoader.beerImage[0].getHeight() / 2.5f;
				
	}
	
	

	//circle around holding drink
	public void drawDrinkCircle(Canvas canvas, float x, float y){
		canvas.drawCircle(x, y, CIRCLE_PAINT_RADIUS, circlePaint);		
		circlePaintAlpha += circlePaintAlphaStep;
		if ((circlePaintAlpha > CIRCLE_PAINT_HIGH_BORDER) || (circlePaintAlpha < CIRCLE_PAINT_LOW_BORDER)) circlePaintAlphaStep = circlePaintAlphaStep * (-1);
		circlePaint.setARGB(circlePaintAlpha, CIRCLE_PAINT_RED, CIRCLE_PAINT_GREEN, CIRCLE_PAINT_BLUE);
	}
	
	
	//showed when the game is ended
	class FinalScreen{

		//private Menu finalScreenMenu = new Menu(context, screenManager, context.getResources().getString(R.string.final_screen_title));
		
		void createFinalScreen(int points){			
			
			finalScreenMenu.clearMenu();
			finalScreenMenu.addMenuLine(new MenuLine(context.getResources().getString(R.string.final_screen_your_score) + " " + Integer.toString(points), false));
			
			if (scoreSQLBase.updateBestScore(points)){
				finalScreenMenu.addMenuLine(new MenuLine(context.getResources().getString(R.string.final_screen_new_high_score), false));
			} else{
					finalScreenMenu.addMenuLine(new MenuLine(context.getResources().getString(R.string.final_screen_not_a_high_score), false));
				}
			finalScreenMenu.addMenuLine(new MenuLine(context.getResources().getString(R.string.button_back), true));
		}
		
		
		public void drawFinalScreen(Canvas canvas){
			finalScreenMenu.drawMenu(canvas);
			
		}
		
		public void setClick(float x, float y){
			finalScreenMenu.checkClick(x, y);
		}
		
		public int getLine(){
			return finalScreenMenu.getChosenLine();
		}
		
	}
	

	
	
	public void addDrankLabel(int points, float intoxication, float x, float y){
		
		drank_label_mass.add(new DrankLabel(points, intoxication, x, y));
		
		//if (!drinksMass.isEmpty()){
		for (int i = 0; i < drank_label_mass.size(); i++){
			if (!drank_label_mass.get(i).checkActive()) drank_label_mass.remove(i);
		}	
		
	}
	
	
	public void drawGUI(Canvas canvas){
		
		timerLabel.drawTimer(canvas, gameTimer.getMinutes(), gameTimer.getSeconds());
		scoreLabel.drawScore(canvas, player.getPoints());
		loadedLabel.drawLoaded(canvas, player.getIntoxicationLevel());
		lastDrinkLabel.drawLabel(canvas);
		
		if (!drank_label_mass.isEmpty()){
			for (int i = 0; i < drank_label_mass.size(); i++)
				drank_label_mass.get(i).drawLabel(canvas);
		}
	
	}
	
	
	//class include information about drank drinks
	class DrankLabel{
		
		final int GOOD_COLOR = Color.argb(255, 0, 191, 0);
		
		private int showingTime = 1500; //msec
		
		int points;
		float x;
		float y;
		float intoxication;
		
		boolean active;
		long start_time;
		
		Paint strokeDrank;
	    Paint textDrank;
		
		DrankLabel(int points, float intoxication, float x, float y){
			this.points = points;
			this.x = x;
			this.y = y;
			this.intoxication = intoxication;
			
			intoxication = intoxication * (255/50); //255 - max of RGB, 60 - maximum intoxication 
			if (intoxication > 255) intoxication = 255;
			
			active = true;
			start_time = System.currentTimeMillis();
			
			strokeDrank = new Paint();
			strokeDrank.setARGB(255, 255, 255, 255);
			strokeDrank.setTextAlign(Paint.Align.CENTER);
			strokeDrank.setStyle(Paint.Style.STROKE);
			strokeDrank.setStrokeWidth(8);
			strokeDrank.setTypeface(screenManager.getTypeFace());
			strokeDrank.setTextSize(screenManager.getTextSize()/2);
			strokeDrank.setAntiAlias(true);
			textDrank = new Paint();
			if (intoxication == 0) textDrank.setColor(GOOD_COLOR);
				else textDrank.setARGB(255, 255, (int)(255 - intoxication), 0);
			textDrank.setTextAlign(Paint.Align.CENTER);
			textDrank.setTypeface(screenManager.getTypeFace());
			textDrank.setTextSize(screenManager.getTextSize()/2);
			textDrank.setAntiAlias(true);
		}
		
		
		public void drawLabel(Canvas canvas){
			
			if ((System.currentTimeMillis() - start_time) > showingTime) active = false;
			else{	
				
			    canvas.drawText("+" + Integer.toString(points), x, y, strokeDrank);
			    canvas.drawText("+" + Integer.toString(points), x, y, textDrank);
			}
		}
		
		public boolean checkActive(){
			return active;
		}
		
	}




class LastDrinkLabel{

    Paint lastDrink;	
    Paint strokeLastDrink;
    String name = "";

    
	public LastDrinkLabel() {
		lastDrink = new Paint();
		lastDrink.setARGB(255, 0, 155, 0);
		lastDrink.setTextAlign(Paint.Align.RIGHT);
		lastDrink.setTypeface(screenManager.getTypeFace());
		lastDrink.setTextSize(screenManager.getTextSize()/2);
		lastDrink.setAntiAlias(true);
		
		strokeLastDrink = new Paint();
		strokeLastDrink.setARGB(255, 255, 255, 255);
		strokeLastDrink.setTextAlign(Paint.Align.RIGHT);
		strokeLastDrink.setStyle(Paint.Style.STROKE);
		strokeLastDrink.setStrokeWidth(8);
		strokeLastDrink.setTypeface(screenManager.getTypeFace());
		strokeLastDrink.setTextSize(screenManager.getTextSize()/2);
		strokeLastDrink.setAntiAlias(true);
	}
	
	
	public void changeLastLabel(String newLabel){
		this.name = newLabel;
	}
	
	
	public void drawLabel(Canvas canvas){
		if (name != ""){
			canvas.drawText(name, screenManager.getScreenWidth() - strokeLastDrink.getStrokeWidth(), screenManager.getScreenHeight() - lastDrink.getTextSize(), strokeLastDrink);
			canvas.drawText(name, screenManager.getScreenWidth() - strokeLastDrink.getStrokeWidth(), screenManager.getScreenHeight() - lastDrink.getTextSize(), lastDrink);
		}
	}
	
}

}



class TimerLabel {
	
	Paint strokeTimerLabel;
    Paint textTimerLabel;
          
    int labelX;
    int labelY;
    
	String minute_text = new String();
	String second_text = new String();
	
	
	public TimerLabel(int screenWidth, int screenHeight, Typeface tf_, int textSize){		
		
		strokeTimerLabel = new Paint();
		strokeTimerLabel.setARGB(255, 255, 255, 255);
		strokeTimerLabel.setTextAlign(Paint.Align.CENTER);
		strokeTimerLabel.setTextSize(textSize - Math.round(textSize/10));
		strokeTimerLabel.setTypeface(tf_);
		strokeTimerLabel.setStyle(Paint.Style.STROKE);
		strokeTimerLabel.setStrokeWidth(8);
		strokeTimerLabel.setAntiAlias(true);

		textTimerLabel = new Paint();
		textTimerLabel.setARGB(255, 86, 147, 216);
		textTimerLabel.setTextAlign(Paint.Align.CENTER);
		textTimerLabel.setTextSize(textSize - Math.round(textSize/10));
		textTimerLabel.setTypeface(tf_);	
		textTimerLabel.setAntiAlias(true);
		
		labelX = Math.round(screenWidth / 2);
		labelY = Math.round(textSize);
	}
	
	
	public void drawTimer(Canvas canvas, int minute, int second){		
		minute_text = "";
		second_text = "";
		
		if (minute < 10) minute_text = Integer.toString(0) + Integer.toString(minute); else minute_text = Integer.toString(minute);
		if (second < 10) second_text = Integer.toString(0) + Integer.toString(second); else second_text = Integer.toString(second);
		
	    canvas.drawText(minute_text + ":" + second_text, labelX, labelY, strokeTimerLabel);
	    canvas.drawText(minute_text + ":" + second_text, labelX, labelY, textTimerLabel);
	}
	
}





class LoadedLabel {
	
	Paint strokeLoadedLabel;
    Paint textLoadedLabel;
          
    int labelX;
    int labelY;
    
    private int red, green, blue;
    private float intoxication_maximum;
    
    private String loadedText = "Loaded";
    private float width;
    private float space, step;
    
    LinearGradient gradient;
    
    private float intoxication_memory;

	
	public LoadedLabel(int screenWidth, int screenHeight, Typeface tf_, int textSize, float intoxication_maximum){		
		
		strokeLoadedLabel = new Paint();
		strokeLoadedLabel.setARGB(255, 255, 255, 255);
		strokeLoadedLabel.setTextAlign(Paint.Align.CENTER);
		strokeLoadedLabel.setTextSize(textSize);
		strokeLoadedLabel.setTypeface(tf_);
		strokeLoadedLabel.setStyle(Paint.Style.STROKE);
		strokeLoadedLabel.setStrokeWidth(8);
		strokeLoadedLabel.setAntiAlias(true);

		textLoadedLabel = new Paint();
		textLoadedLabel.setARGB(255, 86, 147, 216);
		textLoadedLabel.setTextAlign(Paint.Align.CENTER);
		textLoadedLabel.setTextSize(textSize);
		textLoadedLabel.setTypeface(tf_);
		textLoadedLabel.setAntiAlias(true);
		
		this.intoxication_maximum = intoxication_maximum;
		
		red = 0;
		green = 230;
		blue = 0;
		
		labelX = Math.round(screenWidth/5);
		labelY = Math.round(textSize);
		
		width = textLoadedLabel.measureText(loadedText);
		space = width / intoxication_maximum; //width of one intoxication point on "Loaded" label
		
		step = 255 / (intoxication_maximum/2); //width of one intoxication point for color of "Loaded" label
		intoxication_memory = -100;
	}
	
	
	
	public void drawLoaded(Canvas canvas, float intoxication){	
		
		if (intoxication != intoxication_memory){		
			changeColor(intoxication);
		
			//creating new gradient every step .... is it right ?! //???????
			gradient = new LinearGradient((labelX - (width/2)-4) + space*intoxication, 0,(labelX - (width/2)-4) + space*intoxication + 0.1f, 0, Color.rgb(red, green, blue), Color.WHITE, TileMode.CLAMP); 
			textLoadedLabel.setShader(gradient);
			
			intoxication_memory = intoxication;
		}
	    canvas.drawText(loadedText, labelX, labelY, strokeLoadedLabel);
	    canvas.drawText(loadedText, labelX, labelY, textLoadedLabel);
	    
	}
	
	
	//method provided change color on "Loaded" label
	private void changeColor(float intoxication){
		
		if (intoxication <= intoxication_maximum){
			if (intoxication <= Math.round(intoxication_maximum/2)){
				red =  (int)(step * intoxication);
				green = 230;
			}
			else {
				red = 250;
				green = (int)(step*(intoxication_maximum - intoxication));		
			}
		}
	}
	
}







class ScoreLabel{
	
	Paint strokeScoreLabel;
    Paint textScoreLabel;
          
    int labelX;
    int labelY;
	
	public ScoreLabel(int screenWidth, int screenHeight, Typeface tf_, int textSize){
		
		strokeScoreLabel = new Paint();
		strokeScoreLabel.setARGB(255, 255, 255, 255);
		strokeScoreLabel.setTextAlign(Paint.Align.CENTER);
		strokeScoreLabel.setTextSize(textSize);
		strokeScoreLabel.setTypeface(tf_);
		strokeScoreLabel.setStyle(Paint.Style.STROKE);
		strokeScoreLabel.setStrokeWidth(8);
		strokeScoreLabel.setAntiAlias(true);

		textScoreLabel = new Paint();
		textScoreLabel.setARGB(255, 228, 96, 188);
		textScoreLabel.setTextAlign(Paint.Align.CENTER);
		textScoreLabel.setTextSize(textSize);
		textScoreLabel.setTypeface(tf_);	
		textScoreLabel.setAntiAlias(true);
		
		labelX = Math.round(4*(screenWidth/5));
		labelY = Math.round(textSize);	
	}
	
	
	public void drawScore(Canvas canvas, int score){
		
	    canvas.drawText("Score: " + Integer.toString(score), labelX, labelY, strokeScoreLabel);
	    canvas.drawText("Score: " + Integer.toString(score), labelX, labelY, textScoreLabel);
		
	}
	
}
