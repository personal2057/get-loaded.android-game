package com.alexgolikov.getloaded;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;



//main manager for creating and controlling all the objects
public class DrinkManager{
	 
	Resources mRes;
  	
	//cursor for object pool
	private ArrayList<Drink> drinksPool;
	private ArrayList<Drink> currentDrinks; 
	private volatile int poolFirst = 0;
	private volatile int poolLast = 0;
	private int poolNextIndex = 1;
	
	//points of speed up for returning the drink
	private static final int ACCELERATE = 8;
	
	//additional acceleration for drinks movement
	private float speedBonus = 0;
	private static long drinking_time = 0;
	
	//variable for checking positions to create new drink
	private float trecker = 0;

	//main massive with drinks
	public CopyOnWriteArrayList<Drink> drinksMass;
	
	Random rand;	
	
	//public volatile boolean holding_monitor = false; 
	
	
	//monitor for controlling current drink
	public volatile Drink current_drink;
	private volatile boolean monitor_state = true;

	
	ScreenManager screenManager;
	Player player;
	GameTimer timer;
	GUI mainGUI;
	

	
	public DrinkManager(Context context, ScreenManager screenManager, Player player, GameTimer timer, GUI mainGUI){
		
		rand = new Random();
		mRes = context.getResources();
		this.screenManager = screenManager;
		this.player = player;
		this.timer = timer;
		this.mainGUI = mainGUI;
		trecker = screenManager.getScreenWidth();
		
		drinksPool = new ArrayList<Drink>();
		createDrinksPool();
		drinksMass = new CopyOnWriteArrayList<Drink>(drinksPool);	
		
		poolFirst = 0;
		poolLast = 0;
		poolNextIndex = 1;

	}

	
	//incrementing pool cursor, in case of cursor has been walked through whole pool - shuffle pool for a change 
	private int poolIncrement(int var){
		int cur = var;
		
		cur = cur + poolNextIndex;
		if (cur > GameModel.getAmountOfDrinksInPool() - 1){
			cur = cur - GameModel.getAmountOfDrinksInPool();
			shuffleDrinksMass();
		}
		return cur;
	}
	
	
	private boolean poolIsEmpty(){
		return poolLast == poolFirst;
	}
	
	
	public void createDrinksPool(){
		drinksPool.clear();
		
		for (int i = 0; i < GameModel.getAmountOfDrinksInPool(); i++){
			drinksPool.add(createDrink());
		}
	}
	
	
	
	
	//factory with random creating of a new drink
	private Drink createDrink(){
		
		Drink drink = null;
		int randomRange = 0;

		// -2 because of inner random for extra points drinks; and minus non-unlocked secret drinks 
		randomRange = HardPartLoader.getDrinksMassive().size() - 2 - (HardPartLoader.getSecretDrinksMassive().size() - SecretDrinksManager.getSecretDrinksAmount());

		switch (rand.nextInt(randomRange) + 1)
		{
			case 1: drink = new VodkaDrink (screenManager.getScreenWidth(),screenManager.getScreenHeight() - screenManager.getDrinkLevel() - HardPartLoader.vodkaImage[0].getHeight(), HardPartLoader.vodkaImage);
					break;
			case 2: drink = new BeerDrink (screenManager.getScreenWidth(),screenManager.getScreenHeight() - screenManager.getDrinkLevel() - HardPartLoader.beerImage[0].getHeight(), HardPartLoader.beerImage);
					break;
			case 3: drink = new CognacDrink (screenManager.getScreenWidth(),screenManager.getScreenHeight() - screenManager.getDrinkLevel() - HardPartLoader.cognacImage[0].getHeight(), HardPartLoader.cognacImage);
					break;
			case 4: drink = new AbsintheDrink (screenManager.getScreenWidth(),screenManager.getScreenHeight() - screenManager.getDrinkLevel() - HardPartLoader.absintheImage[0].getHeight(), HardPartLoader.absintheImage);
					break;
			case 5: drink = new B52Drink (screenManager.getScreenWidth(),screenManager.getScreenHeight() - screenManager.getDrinkLevel() - HardPartLoader.b_52Image[0].getHeight(), HardPartLoader.b_52Image);
					break;
			case 6: drink = new BloodMaryDrink (screenManager.getScreenWidth(),screenManager.getScreenHeight() - screenManager.getDrinkLevel() - HardPartLoader.bloodmaryImage[0].getHeight(), HardPartLoader.bloodmaryImage);
					break;
			case 7: drink = new ChampagneDrink (screenManager.getScreenWidth(),screenManager.getScreenHeight() - screenManager.getDrinkLevel() - HardPartLoader.champagneImage[0].getHeight(), HardPartLoader.champagneImage);
					break;
			case 8: drink = new LongIslandDrink (screenManager.getScreenWidth(),screenManager.getScreenHeight() - screenManager.getDrinkLevel() - HardPartLoader.longislandImage[0].getHeight(), HardPartLoader.longislandImage);
					break;
			case 9: drink = new MargaritaDrink (screenManager.getScreenWidth(),screenManager.getScreenHeight() - screenManager.getDrinkLevel() - HardPartLoader.margaritaImage[0].getHeight(), HardPartLoader.margaritaImage);
					break;
			case 10: drink = new MojitoDrink (screenManager.getScreenWidth(),screenManager.getScreenHeight() - screenManager.getDrinkLevel() - HardPartLoader.mojitoImage[0].getHeight(), HardPartLoader.mojitoImage);
					break;
			case 11: drink = new TequilaDrink (screenManager.getScreenWidth(),screenManager.getScreenHeight() - screenManager.getDrinkLevel() - HardPartLoader.tequilaImage[0].getHeight(), HardPartLoader.tequilaImage);
					break;
			case 12: drink = new WineDrink (screenManager.getScreenWidth(),screenManager.getScreenHeight() - screenManager.getDrinkLevel() - HardPartLoader.wineImage[0].getHeight(), HardPartLoader.wineImage);
					break;
			case 13: drink = new UnknownDrink (screenManager.getScreenWidth(), screenManager.getScreenHeight() - screenManager.getDrinkLevel() - HardPartLoader.unknownImage[0].getHeight(), HardPartLoader.unknownImage);
					break;
			case 14: {
						int i = rand.nextInt(10) + 1;
						if (i <= 5)
							drink = new Points100Drink (screenManager.getScreenWidth(),screenManager.getScreenHeight() - screenManager.getDrinkLevel() - HardPartLoader.points100Image[0].getHeight(), HardPartLoader.points100Image);
							else 
								if (i >= 9)
									drink = new Points500Drink (screenManager.getScreenWidth(),screenManager.getScreenHeight() - screenManager.getDrinkLevel() - HardPartLoader.points500Image[0].getHeight(), HardPartLoader.points500Image);
									else  
										drink = new Points250Drink (screenManager.getScreenWidth(),screenManager.getScreenHeight() - screenManager.getDrinkLevel() - HardPartLoader.points250Image[0].getHeight(), HardPartLoader.points250Image);
			}
					break;
			
			case 15: drink = new ButterBeer (screenManager.getScreenWidth(), screenManager.getScreenHeight() - screenManager.getDrinkLevel() - HardPartLoader.secretDrink1[0].getHeight(), HardPartLoader.secretDrink1, HardPartLoader.secretDrink1);
		}		

		return drink;		
	}
	
	
	
	
	public boolean checkMonitorState(){
		return monitor_state;
	}
	
	
	
	
	public void setMonitorState(boolean state){
		monitor_state = state;
	}


	//shuffle massive of drinks for a change 
	private void shuffleDrinksMass(){	
		for (int i = 0; i < GameModel.SHUFFLING_POOL_STEPS; i++){
			swap(getProperIndex(), getProperIndex());
		}	
	}
	
	
	//get index for swapping element in massive (should not be between first and last drink in pool) 
	private int getProperIndex(){
		
		int index = rand.nextInt(GameModel.DRINKS_COUNT_IN_POOL);
		
		if (poolLast - poolFirst > 0){
			while((index >= poolFirst) && (index <= poolLast)){
				index = rand.nextInt(GameModel.DRINKS_COUNT_IN_POOL);
			}
		}
			else{
				while((index <= poolLast) || (index >= poolFirst)){
					index = rand.nextInt(GameModel.DRINKS_COUNT_IN_POOL);
				}	
			}		
		
		return index;
	}
	
	
	private void swap(int i, int j){
		Drink temp;
		
		temp = drinksMass.get(i);
		drinksMass.set(i, drinksMass.get(j));
		drinksMass.set(j, temp);
	}
	
	
	
	public void addDrink(){
		poolLast = poolIncrement(poolLast);
		drinksMass.get(poolLast).resetState();
	}
	
	
	
	
	private void deleteDrink(int i){
		if (!poolIsEmpty()) poolFirst = poolIncrement(poolFirst);
	}
	
	
	
	public void changeFrames(){
		
		if (!poolIsEmpty()){
			int i = poolFirst;

			while (i != poolLast){
				drinksMass.get(i).changeFrame();
				i = poolIncrement(i);
			}
		}
	}
	
	
	
	public ArrayList<Drink> getCurrentDrinksAsList(){
		
		currentDrinks.clear();
		
		if (!poolIsEmpty()){
			int i = poolFirst;

			while (i != poolLast){
				currentDrinks.add(drinksMass.get(i));
				i = poolIncrement(i);
			}
		}
		
		return currentDrinks;
		
	}
	
	
	
	//drawing all the drinks on canvas
	public void drawDrinks(Canvas canvas){	
		
		if (!poolIsEmpty()){
			//if the drink is sliding
			int i = poolFirst;
			synchronized (this) {
				while (i != poolLast){
					if (drinksMass.get(i).getCurrentState() == Drink.STATE_SLIDE) 
						canvas.drawBitmap(drinksMass.get(i).drinkImage[drinksMass.get(i).getFrame()], drinksMass.get(i).position_x, drinksMass.get(i).position_y, null);
					i = poolIncrement(i);
				}	
			}
				
			//if the drink is returning to its place
			i = poolFirst;
			while (i != poolLast){
				if (drinksMass.get(i).getCurrentState() == Drink.STATE_RETURN)
					canvas.drawBitmap(drinksMass.get(i).drinkImage[drinksMass.get(i).getFrame()], drinksMass.get(i).holding_x, drinksMass.get(i).holding_y, null);
				i = poolIncrement(i);
			}
					
			//if we have got a holding drink
			if (!checkMonitorState()){	
				mainGUI.drawDrinkCircle(canvas, current_drink.holding_x + (current_drink.getImageWidth()/2), current_drink.holding_y + (current_drink.getImageHeight()/2));
				canvas.drawBitmap(current_drink.drinkImage[current_drink.getFrame()], current_drink.holding_x, current_drink.holding_y, null);
			}
		}

	}
	
	
	
	public void increaseSpeed(float speed){
				
		if (speedBonus < (screenManager.getDrinkSpeed() * 3)) speedBonus += speed;
		
	}
	
	
	
	public void makeStep(float step){
		
		if (!poolIsEmpty()){
			
			synchronized(this){
				int i = poolFirst;
			
				while (i != poolLast){
					drinksMass.get(i).position_x = drinksMass.get(i).position_x - step - speedBonus;
			
				if (drinksMass.get(i).getCurrentState() == Drink.STATE_RETURN)
					calculatePoints(drinksMass.get(i), step);
	
				if (drinksMass.get(i).position_x < drinksMass.get(i).getImageWidth()*(-1)) deleteDrink(i);
					i = poolIncrement(i);
				}
			}
		}
		
		//creating new drink	
		trecker = trecker - step - speedBonus;
		if ((screenManager.getScreenWidth() - trecker) > screenManager.getDrinkSpace()){
			addDrink();
			trecker = screenManager.getScreenWidth();
		}
		
	}
	
	
	
	//method for return the drink to its original points
	private void calculatePoints(Drink drink, float step){
		
		
		double angle, a, b;
		
		if (drink.holding_y > drink.position_y)
			b = drink.holding_y - drink.position_y;
			else b = drink.position_y - drink.holding_y;
		
		if (drink.position_x > drink.holding_x)
			a = drink.position_x -drink.holding_x;
			else a = drink.holding_x -drink.position_x;
		
		//check if the drink can be already returned to its place
		if ((b < Math.round(drink.getImageHeight()/2)) && (a < Math.round(drink.getImageWidth()/2))){
			drink.holding_x = drink.position_x;
			drink.holding_y = drink.position_y;
			drink.setCurrentState(Drink.STATE_SLIDE);
		}
		

		angle = Math.atan(b / a);
		
		
		if ((drink.position_x < drink.holding_x)&&(drink.position_y < drink.holding_y)){
			
				drink.holding_x = drink.holding_x - (float)((ACCELERATE*step) * Math.cos(angle));
				drink.holding_y = drink.holding_y - (float)((ACCELERATE*step) * Math.sin(angle));
			}
		else 
			if ((drink.position_x < drink.holding_x)&&(drink.position_y > drink.holding_y)){
				
				drink.holding_x = drink.holding_x - (float)((ACCELERATE*step) * Math.cos(angle));
				drink.holding_y = drink.holding_y + (float)((ACCELERATE*step) * Math.sin(angle));
			}
			else 
				if ((drink.position_x > drink.holding_x)&&(drink.position_y > drink.holding_y)){
					
					drink.holding_x = drink.holding_x + (float)((ACCELERATE*step) * Math.cos(angle));
					drink.holding_y = drink.holding_y + (float)((ACCELERATE*step) * Math.sin(angle));
				}
				else
					if ((drink.position_x > drink.holding_x)&&(drink.position_y < drink.holding_y)){
						
						drink.holding_x = drink.holding_x + (float)((ACCELERATE*step) * Math.cos(angle));
						drink.holding_y = drink.holding_y - (float)((ACCELERATE*step) * Math.sin(angle));
					} 
		
		
	}
	
	
	
	
	//method for controlling first click on the drink
	public void checkClick(float x, float y){
		
		
		if ((!poolIsEmpty()) && (checkMonitorState())){

			int i = poolFirst;
			while (i != poolLast){
				if ((drinksMass.get(i).getCurrentState() == Drink.STATE_RETURN) 
						&& (drinksMass.get(i).holding_x < x) 
						&& (drinksMass.get(i).holding_x + drinksMass.get(i).getImageWidth() > x)
						&& (drinksMass.get(i).holding_y < y) 
						&& (drinksMass.get(i).holding_y + drinksMass.get(i).getImageHeight() > y))
							{
								setMonitorState(false);
								current_drink = drinksMass.get(i);
								checkHold(x, y);
								break;
							}
				
				if ((drinksMass.get(i).getCurrentState() == Drink.STATE_SLIDE) 
					&& (drinksMass.get(i).position_x < x) 
					&& (drinksMass.get(i).position_x + drinksMass.get(i).getImageWidth() > x)
					&& (drinksMass.get(i).position_y < y) 
					&& (drinksMass.get(i).position_y + drinksMass.get(i).getImageHeight() > y))
						{
							setMonitorState(false);
							current_drink = drinksMass.get(i);
							checkHold(x, y);
							break;
						}
				i = poolIncrement(i);
			}
		}
	}
	
	
	
	//method for checking holding the drink state
	public void checkHold(float x, float y){
		
		if (!checkMonitorState()){			
			if (current_drink.getCurrentState() != Drink.STATE_HOLD) current_drink.setCurrentState(Drink.STATE_HOLD);
			
			current_drink.setHoldingPosition(x - Math.round(current_drink.getImageWidth()/2), y - Math.round(current_drink.getImageHeight()/2));
					
			//don't let the drink to cross the screen border
			if (x - Math.round(current_drink.getImageWidth()/2) < 0) current_drink.setHoldingX(0);
				else if (x + Math.round(current_drink.getImageWidth()/2) > screenManager.getScreenWidth()) current_drink.setHoldingX((float)screenManager.getScreenWidth() - current_drink.getImageWidth());
			if (y - Math.round(current_drink.getImageHeight()/2) < 0) current_drink.setHoldingY(0);
				else if (y + Math.round(current_drink.getImageHeight()/2) > screenManager.getScreenHeight()) current_drink.setHoldingY((float)screenManager.getScreenHeight() - Math.round(current_drink.getImageHeight()));
			
			//if the drink has crossed the border - its being drunk!
			if (current_drink.holding_y < screenManager.getLineOfDrinking()){
				checkDrink();
			}
		}
	}
	
	
	
	//method for returning the drink to position
	public void checkReturn(float x, float y){
		
		if (!checkMonitorState()){
			current_drink.setCurrentState(Drink.STATE_RETURN);
			setMonitorState(true);
			//current_drink = null;
		}
	}
	
	
	//method for drinking the drink
	public void checkDrink(){

		current_drink.setCurrentState(Drink.STATE_DRINK);
		
		if (current_drink.getId() == Drink.UNKNOWN_DRINK){	
			
			float new_holding_x = current_drink.holding_x;
			synchronized (this) {
				while (current_drink.getId() == Drink.UNKNOWN_DRINK){
					current_drink = HardPartLoader.getDrinksMassive().get(rand.nextInt(HardPartLoader.getDrinksMassive().size() - 1));
				}
			
				player.setPoints(current_drink.getPoints() * GameModel.POWER_FOR_UNKNOWN);
				mainGUI.addDrankLabel(current_drink.getPoints() * GameModel.POWER_FOR_UNKNOWN, player.getLastIntoxication(), new_holding_x, screenManager.getTextSize() + (current_drink.getImageHeight()/2));
			}
		}
		else{
			player.setPoints(current_drink.getPoints());
			mainGUI.addDrankLabel(current_drink.getPoints(), player.getLastIntoxication(), current_drink.holding_x, screenManager.getTextSize() + (current_drink.getImageHeight()/2));
		}	
		
		
		player.setIntoxicationLevel(current_drink, (timer.getMinutes()*60) + timer.getSeconds());
			
		if (current_drink.isDrink()){
			setDrinkingTime(System.currentTimeMillis());
			timer.setPenaltyReady(false);
		}
		
		//label with last drunk drink
		GUI.lastDrinkLabel.changeLastLabel(current_drink.getName());
		
		//we have no drink in hand already
		setMonitorState(true);
	}
	

	public static void setDrinkingTime(long time){
		drinking_time = time;
	}	
	
	
	public static long getDrinkingTime(){
		return drinking_time;
	}
	
	
}
	