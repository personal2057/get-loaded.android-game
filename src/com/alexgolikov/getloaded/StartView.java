package com.alexgolikov.getloaded;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;



public class StartView extends SurfaceView implements SurfaceHolder.Callback {
	
	private StartThread thread;
	SurfaceHolder holder;
	Context context;
	Activity activity;
	
	private Bitmap mBackgroundImage; 
	private ScreenManager screenManager;
	
	int text_height = 0;
	int text_width = 0;
	
	Menu menu;
	
	Intent intentGame;
	Intent intentBarMenu;
	Intent intentAchievements;
	
	MySQLiteHelper scoreBase;
	
    public StartView(Context context, Activity activity) {
        super(context);
        
        this.activity = activity;
        this.context = context;
        holder = getHolder();
        holder.addCallback(this);          
        scoreBase = new MySQLiteHelper(context);
        
    	intentGame = new Intent(activity, GameActivity.class);
    	intentBarMenu = new Intent(activity, BarMenuActivity.class);
    	intentAchievements = new Intent(activity, AchievementsActivity.class);
        
    } 
    
    
    
    
    class StartThread extends Thread{
    	
    	private SurfaceHolder mSurfaceHolder;
    	private boolean runFlag = false;
    	

    	
    	public StartThread(SurfaceHolder surfaceHolder, Context context, Handler handler){
    		mSurfaceHolder = surfaceHolder;
    		screenManager = new ScreenManager(context);
            mBackgroundImage = HardPartLoader.background_0;
    		mBackgroundImage = Bitmap.createScaledBitmap(mBackgroundImage, screenManager.getScreenWidth(), screenManager.getScreenHeight(), true);
    	    		    
    		//add unlocked secret drinks to static class 
    		SecretDrinksManager.setDrinksList(scoreBase.getUnlockedSecretDrinks());
    		
    		menu = new Menu(context, screenManager, context.getResources().getString(R.string.start_screen_lable));
    		
    		menu.addMenuLine(new MenuLine(context.getResources().getString(R.string.start_screen_start), true));
    		menu.addMenuLine(new MenuLine(context.getResources().getString(R.string.start_screen_bar_menu), true));
    		menu.addMenuLine(new MenuLine(context.getResources().getString(R.string.start_screen_achievements), true));
    		menu.addMenuLine(new MenuLine(context.getResources().getString(R.string.start_screen_best_score) + " " + Integer.toString(scoreBase.getBestScore()), false));
    		menu.addMenuLine(new MenuLine(context.getResources().getString(R.string.start_screen_exit), true));
    	}
    
        
    	public void setRunning(boolean run) {
            runFlag = run;
        }
    	
    	
    	
    	public void run(){
    	
    		Canvas c = null;
    		
            while (runFlag) {
            	
            	if (mSurfaceHolder.getSurface().isValid()){
            		try{
            			c = mSurfaceHolder.lockCanvas();

            			synchronized (mSurfaceHolder){         					
            				c.drawBitmap(mBackgroundImage, 0, 0, null);	      				
            				menu.drawMenu(c);        				
            			}
            		}
            		finally{
            			if (c != null) mSurfaceHolder.unlockCanvasAndPost(c);
            		}
            	}
            }
    	}
    
    }
    
    
    
    public boolean onTouchEvent(MotionEvent event) {
    	
    	switch (event.getAction()){
    	
    		case MotionEvent.ACTION_DOWN: {
    			 menu.checkClick(event.getX(0), event.getY(0));		
    		};
    			 break;
    		
    		case MotionEvent.ACTION_MOVE: {};
    			 break;
    			 
    		case MotionEvent.ACTION_UP:   {
    			
    			switch (menu.getChosenLine()){
    				case 1: {
    					//Intent intent = new Intent(activity, GameActivity.class);
    					activity.startActivity(intentGame);
    				};
    				break;
    				
    				case 2: {
    					
    					intentBarMenu.putExtra("textsize", screenManager.getTextSize());
    					activity.startActivityForResult(intentBarMenu, 0);
    				};
    				break;
    				
    				case 3: {
    					
    					intentAchievements.putExtra("textsize", screenManager.getTextSize());
    					activity.startActivityForResult(intentAchievements, 0);
    				};
    				break;
    				
    				case 5:{
    					onPauseSurfaceView();
    					//activity.finish();
    					System.exit(0);
    				}
    				break;
    			}
    		};
    			 break;
    	}
    	
    	return true;
    }
    
    
    
    public StartThread getThread() {
        return thread;
    }
    
        
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {	
    }
 
    
    public void surfaceCreated(SurfaceHolder holder) {
 
    }
    
    
    public void surfaceDestroyed(SurfaceHolder holder) {

    }
    
    
    
    public void onResumeSurfaceView(){
       	thread = new StartThread(holder, getContext(), new Handler());
       	GameModel.setGameState(GameModel.STATE_START);
       	GameModel.setHowToScreenShowed(false);
        thread.setRunning(true);
        thread.start();
    }
    
    
    public void onPauseSurfaceView(){
        boolean retry = true;
        thread.setRunning(false);
        
        
        while (retry) {
            try {
                thread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
    }
}
