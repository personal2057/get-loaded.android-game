package com.alexgolikov.getloaded;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;


//Class for universal menu for application
public class Menu {

	private ScreenManager screenManager;
	private Context context;
	//massive of menu lines
	ArrayList<MenuLine> menuLines = new ArrayList<MenuLine>();
	ArrayList<Integer> menuPositions = new ArrayList<Integer>();
	
	//margin to screen borders for menu lines (prevent too wide lines in menu), only for title and non-buttons 
	private final int MENU_LINE_MARGIN = 4;
	
	//title of the menu
	String title;
	
	Paint titlePaint;
	Paint titleStrokePaint; 
	
	Paint activeLinePaint;
	Paint linePaint;
	Paint pressedActiveLinePaint;
	Paint lineStrokePaint; 
	
	int space;
	int menuArea;
	
	Rect bounds;
	int text_width;
	
	int chosenLine = 0;
	
	public Menu(Context context, ScreenManager screenManager, String title){
		
		this.screenManager = screenManager;
		this.context = context;
		menuLines.clear();
		menuPositions.clear();
		this.title = title;
		
		//space for titles and menu lines
		space = screenManager.getScreenHeight() / 20;
		
		titleStrokePaint = new Paint();
		titleStrokePaint.setARGB(255, 255, 255, 255);
		titleStrokePaint.setTextAlign(Paint.Align.CENTER);
		titleStrokePaint.setStyle(Paint.Style.STROKE);
		titleStrokePaint.setStrokeWidth(8);
		titleStrokePaint.setTypeface(screenManager.getTypeFace());
		titleStrokePaint.setTextSize(screenManager.getTextSize() * 1.5f);
		titleStrokePaint.setAntiAlias(true);
		titlePaint = new Paint();
		titlePaint.setShadowLayer(0.6f, 1.5f, 1.5f, context.getResources().getColor(R.color.shadow_color));
		titlePaint.setColor(context.getResources().getColor(R.color.title));
		titlePaint.setTextAlign(Paint.Align.CENTER);
		titlePaint.setTypeface(screenManager.getTypeFace());
		titlePaint.setTextSize(screenManager.getTextSize() * 1.5f);
		titlePaint.setAntiAlias(true);
		
		lineStrokePaint = new Paint();
		lineStrokePaint.setARGB(255, 255, 255, 255);
		lineStrokePaint.setTextAlign(Paint.Align.CENTER);
		lineStrokePaint.setStyle(Paint.Style.STROKE);
		lineStrokePaint.setStrokeWidth(5);
		lineStrokePaint.setTypeface(screenManager.getTypeFace());
		lineStrokePaint.setTextSize(screenManager.getTextSize());
		lineStrokePaint.setAntiAlias(true);
		
		activeLinePaint = new Paint();
		activeLinePaint.setShadowLayer(0.6f, 1.5f, 1.5f, context.getResources().getColor(R.color.shadow_color));
		activeLinePaint.setColor(context.getResources().getColor(R.color.active_button));
		activeLinePaint.setTextAlign(Paint.Align.CENTER);
		activeLinePaint.setTypeface(screenManager.getTypeFace());
		activeLinePaint.setTextSize(screenManager.getTextSize());
		activeLinePaint.setAntiAlias(true);
		
		pressedActiveLinePaint = new Paint();
		pressedActiveLinePaint.setShadowLayer(0.6f, 1.5f, 1.5f, context.getResources().getColor(R.color.shadow_color));
		pressedActiveLinePaint.setColor(context.getResources().getColor(R.color.active_button_pressed));
		pressedActiveLinePaint.setTextAlign(Paint.Align.CENTER);
		pressedActiveLinePaint.setTypeface(screenManager.getTypeFace());
		pressedActiveLinePaint.setTextSize(screenManager.getTextSize());
		pressedActiveLinePaint.setAntiAlias(true);
		
		linePaint = new Paint();
		linePaint.setShadowLayer(0.6f, 1.5f, 1.5f, context.getResources().getColor(R.color.shadow_color));
		linePaint.setColor(context.getResources().getColor(R.color.plain_string));
		linePaint.setTextAlign(Paint.Align.CENTER);
		linePaint.setTypeface(screenManager.getTypeFace());
		linePaint.setTextSize(screenManager.getTextSize());
		linePaint.setAntiAlias(true);
		
		menuArea = screenManager.getScreenHeight() - screenManager.getTextSize() - (space*3);
		bounds = new Rect();
	}
	
	
	
	public void clearMenu(){
		menuLines.clear();
		menuPositions.clear();
	}
	
	
	
	public void addMenuLine(MenuLine menuLine){
		
		menuLines.add(menuLine);
		menuPositions.clear();
		
		for (int i = 0; i < menuLines.size(); i++)
			menuPositions.add((screenManager.getScreenHeight() - menuArea) + ((i + 1) * (menuArea / (menuLines.size()+1))));
		
	}
	
	
	public void drawMenu(Canvas c){
		
		float currentTextSize;
		
		//painting menu title
		currentTextSize = titleStrokePaint.getTextSize();
		while (titleStrokePaint.measureText(title) > screenManager.getScreenWidth() - (screenManager.getScreenWidth()/MENU_LINE_MARGIN)){
			--currentTextSize;
			titleStrokePaint.setTextSize(currentTextSize);
		}	
		titlePaint.setTextSize(currentTextSize);
		c.drawText(title, (screenManager.getScreenWidth()/2), screenManager.getTextSize() + space*2, titleStrokePaint);
	    c.drawText(title, (screenManager.getScreenWidth()/2), screenManager.getTextSize() + space*2, titlePaint);
	    
	    
	    if (!menuLines.isEmpty()){    	
	    	float textSizeBuf = linePaint.getTextSize();
	    	
	    	for (int i = 0; i < menuLines.size(); i++){
	    		if (menuLines.get(i).isActive() && !(menuLines.get(i).isPressed())) c.drawText(menuLines.get(i).getText(), (screenManager.getScreenWidth()/2), menuPositions.get(i), activeLinePaint);
	    			else 
	    			if (menuLines.get(i).isActive() && (menuLines.get(i).isPressed())) c.drawText(menuLines.get(i).getText(), (screenManager.getScreenWidth()/2), menuPositions.get(i), pressedActiveLinePaint);
	    				else{
	    					currentTextSize = linePaint.getTextSize();
	    					while (linePaint.measureText(menuLines.get(i).getText()) > screenManager.getScreenWidth() - (screenManager.getScreenWidth()/MENU_LINE_MARGIN)){
	    						--currentTextSize;
	    						linePaint.setTextSize(currentTextSize);
	    					}
	    					c.drawText(menuLines.get(i).getText(), (screenManager.getScreenWidth()/2), menuPositions.get(i), linePaint);
	    					linePaint.setTextSize(textSizeBuf);
	    				}
	    	}
	    }
	}
	
	
	public void checkClick(float x, float y){
	
		chosenLine = 0;
		
		if (!menuPositions.isEmpty()){
			for (int i = 0; i < menuPositions.size(); i++){
				
				if ((Math.round(y) > menuPositions.get(i) - screenManager.getTextSize()) && (Math.round(y) < menuPositions.get(i))){
    				linePaint.getTextBounds(menuLines.get(i).getText(), 0, menuLines.get(i).getText().length(), bounds);
    				text_width =  bounds.width();
    				
    				if ((Math.round(x) > screenManager.getScreenWidth()/2 - text_width/2) && (Math.round(x) < screenManager.getScreenWidth()/2 + text_width/2)){
    					menuLines.get(i).setPressed(true);
    					chosenLine = i + 1;
    				}
				}
			}
		}	
	}
	
	
	public int getChosenLine(){
		
		for (int i = 0; i < menuLines.size(); i++){
			if (menuLines.get(i).isPressed()) menuLines.get(i).setPressed(false);
		}
		
		return chosenLine;
	}
	
	
}


//class of line for menu
class MenuLine{
	
	private Boolean active;
	private String text;
	
	private Boolean pressed;
	
	MenuLine(String text, Boolean active){
		this.text = text;
		this.active = active;

		pressed = false;
	}
	
	
	//Check if this just a text or a button
	public boolean isActive(){
		return active;
	}
	
	
	public String getText(){
		return text;
	}
	
	//if buttons is being pressed
	public boolean isPressed(){
		return pressed;
	}
	
	//set button state
	public void setPressed(boolean pressed){
		this.pressed = pressed;
	}
	
}
