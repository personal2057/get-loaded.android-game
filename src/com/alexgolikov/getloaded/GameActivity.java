package com.alexgolikov.getloaded;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

public class GameActivity extends Activity {

	private GameView mGameView;
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);   
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        setContentView(R.layout.activity_start);
             
        mGameView = new GameView(this, this);    
        setContentView(mGameView);
        
    }
    

    
    
    @Override
    protected void onPause() {
        super.onPause();
        
        mGameView.onPauseSurfaceView();
       }

    
    
    
    @Override
    protected void onResume() {
        super.onResume();
        
        mGameView.onResumeSurfaceView();
    }
    
    
    
    
    @Override
    public void onStop()
    {
    	
        super.onStop();
    }
    
   
    
    @Override
    public void onDestroy()
    {
        super.onDestroy();

    }
    
}

