package com.alexgolikov.getloaded;

import java.util.HashSet;
import java.util.Set;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;



//DataBase for storage main data of game
class MySQLiteHelper extends SQLiteOpenHelper {
	 
  private static final int DATABASE_VERSION = 7;
  private static final String DATABASE_NAME = "LoadedDB";
  
  //Table of unlocked achievements
  private static final String TABLE_ACHIEVEMENTS = "achievements";
  private static final String KEY_NUM = "num";
  private static final String KEY_ID = "id";
  
  //Table of secret drinks
  private static final String TABLE_SECRET_DRINKS = "secret_drinks";
  private static final String KEY_DRINK_NUM = "num";
  private static final String KEY_DRINK_ID = "id";
  
  //Table for storing best score
  private static final String TABLE_SCORE = "score";
  private static final String KEY_SCORE_NUM = "score_num";
  private static final String BEST_SCRORE = "score_best";
  
  //Table for storing information about sound
  private static final String TABLE_SOUND = "sound";  
  private static final String KEY_SOUND_NUM = "sound_num";
  private static final String SOUND_ENABLE = "sound_enable";

  
  private SQLiteDatabase db;
  private Cursor c;
  private HashSet<Integer> unlockedAchievements;
  
  public MySQLiteHelper(Context context) {
      super(context, DATABASE_NAME, null, DATABASE_VERSION); 
      
      unlockedAchievements = new HashSet<Integer>();
      unlockedAchievements.clear();
  }
  
 
  

  @Override
  public void onCreate(SQLiteDatabase db) {
  	
  	//id of unlocked achievements just storage in database (column id)
  	String CREATE_ACHIEVEMENTS_TABLE = "CREATE TABLE " + TABLE_ACHIEVEMENTS + "(" +
  			KEY_NUM + " integer PRIMARY KEY AUTOINCREMENT," + 
  			KEY_ID + " integer)";
  	
  	//id of secret drinks just storage in database (column id)
  	String CREATE_SECRET_DRINKS_TABLE = "CREATE TABLE " + TABLE_SECRET_DRINKS + "(" +
  			KEY_DRINK_NUM + " integer PRIMARY KEY AUTOINCREMENT," + 
  			KEY_DRINK_ID + " integer)";

  	//best user score storing in the table
  	String CREATE_SCORE_TABLE = "CREATE TABLE " + TABLE_SCORE + "(" +
  			KEY_SCORE_NUM + " integer PRIMARY KEY AUTOINCREMENT," + 
  			BEST_SCRORE + " integer)";
  	
  	//sound options storing in the table
  	String CREATE_SOUND_TABLE = "CREATE TABLE " + TABLE_SOUND + "(" +
  			KEY_SOUND_NUM + " integer PRIMARY KEY AUTOINCREMENT," + 
  			SOUND_ENABLE + " integer)";

      db.execSQL(CREATE_ACHIEVEMENTS_TABLE);
      db.execSQL(CREATE_SECRET_DRINKS_TABLE);
      db.execSQL(CREATE_SCORE_TABLE);
      db.execSQL(CREATE_SOUND_TABLE);
      
      //adding only one string with best score
      ContentValues values = new ContentValues();
      values.put(BEST_SCRORE, 0);
      db.insert(TABLE_SCORE, null, values);
  }


  
  //check had achievement been unlocked (by finding id in base), if hadn't - add id of achievement into the base 
  public boolean checkAchievemntInDB(int id){
  	
  	boolean isNew = true;
  	
  	db = this.getWritableDatabase();  
  	c = db.rawQuery("SELECT * FROM " + TABLE_ACHIEVEMENTS + " WHERE " + KEY_ID + "= ?" , new String[]{String.valueOf(id)});
  	
  	if (c.getCount() > 0) isNew = false; 
  	else{
      	ContentValues values = new ContentValues();
      	values.put(KEY_ID, id); 

      	db.insert(TABLE_ACHIEVEMENTS, null, values);
      	isNew = true;
  	}
  	
  	c.close();
  	db.close();
  	
  	return isNew;
  }
  
  
  //add secret drink to the base 
  public void addSecretDrinkInDB(int id){
  	
  	db = this.getWritableDatabase();  
  	c = db.rawQuery("SELECT * FROM " + TABLE_SECRET_DRINKS + " WHERE " + KEY_DRINK_ID + "= ?" , new String[]{String.valueOf(id)});
  	
  	if (!(c.getCount() > 0)){
  		ContentValues values = new ContentValues();
      	values.put(KEY_DRINK_ID, id); 

      	db.insert(TABLE_SECRET_DRINKS, null, values);
  	}
  	
  	c.close();
  	db.close();
 
  }
  
  
  
  
  public Set<Integer> getUnlockedSecretDrinks(){
	  
	  HashSet<Integer> unlockedSecretDrinks = new HashSet<Integer>();
	  
	  db = this.getReadableDatabase(); 
	  c = db.query(TABLE_SECRET_DRINKS, new String[] {KEY_DRINK_ID}, null, null, null, null, null);
	  	
	  if (c.moveToFirst()){
		  do{
			  unlockedSecretDrinks.add(Integer.parseInt(c.getString(0)));
		  }
		  while (c.moveToNext());
	  }
	  	
	  c.close();
	  db.close();  
	  
	  return unlockedSecretDrinks;
  }
  
  
  
  public Set<Integer> getUnlockedAchievements(){
	  
	  unlockedAchievements.clear();
	  
	  db = this.getReadableDatabase(); 
	  c = db.query(TABLE_ACHIEVEMENTS, new String[] {KEY_ID}, null, null, null, null, null);
	  	
	  if (c.moveToFirst()){
		  do{
			  unlockedAchievements.add(Integer.parseInt(c.getString(0)));
		  }
		  while (c.moveToNext());
	  }
	  	
	  c.close();
	  db.close();  
	  
	  return unlockedAchievements;
  }
  
  
  
  
  public boolean updateBestScore(int new_score){
	  
	  boolean result = false;
	  
	  db = this.getWritableDatabase();
	  c = db.rawQuery("SELECT * FROM " + TABLE_SCORE, null);

	  if (c.getCount() > 0){
		  c.moveToFirst();
		  
		  if (c.getInt(1) < new_score){		  
			  ContentValues newValues = new ContentValues();
			  newValues.put(BEST_SCRORE, new_score);

			  db.update(TABLE_SCORE, newValues, KEY_SCORE_NUM + "=" + Integer.toString(c.getInt(0)), null);
			  result = true;			  
		  }
		  else result = false;
	  }

      c.close();
      db.close();	 
      return result;
      
  }
  
  
  //pull out best score from the base
  public int getBestScore(){
	  
	  int result = 0;
	  
	  db = this.getWritableDatabase();
	  c = db.rawQuery("SELECT * FROM " + TABLE_SCORE, null);
	  
	  if (c.getCount() > 0){
		  c.moveToFirst();
		  result =  c.getInt(1); 
	  }
	  	else result = -100;
	  
	  c.close();
	  db.close();
      return result; 
	  
  }
  
  
 
  
  
  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

      db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACHIEVEMENTS);
      db.execSQL("DROP TABLE IF EXISTS " + TABLE_SECRET_DRINKS);
      db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCORE);
      db.execSQL("DROP TABLE IF EXISTS " + TABLE_SOUND);
      onCreate(db);
  }

}



