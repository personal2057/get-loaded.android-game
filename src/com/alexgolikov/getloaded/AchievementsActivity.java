package com.alexgolikov.getloaded;


import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class AchievementsActivity extends Activity {

	private EventManager eventManager;
	private StableArrayAdapter adapter;
	private MySQLiteHelper achivementsBase;
	private Set<Integer> achivementsSet = new HashSet<Integer>();
	
	private int textsize = 0;
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);   
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        setContentView(R.layout.achievements_list_layout);
        
        eventManager = new EventManager();
        achivementsBase = new MySQLiteHelper(this);
        
        achivementsSet.clear();
        achivementsSet.addAll(achivementsBase.getUnlockedAchievements());
        
        textsize = this.getIntent().getExtras().getInt("textsize");
        
        ListView listview = (ListView) findViewById(R.id.listview_achievements);
        TextView text_total = (TextView) findViewById(R.id.total);
        TextView text_back = (TextView) findViewById(R.id.back);
        
        text_total.setText(Integer.toString(achivementsSet.size()) + '/' + Integer.toString(eventManager.getAchievementsLabelList().size())); 
        text_total.setTextSize(textsize/3);
        text_total.setTypeface(HardPartLoader.tf);
        text_total.setTextColor(getResources().getColor(R.color.active_button));
        text_total.setBackgroundColor(Color.TRANSPARENT);
        
        text_back.setTextSize(textsize/3);
        text_back.setTypeface(text_total.getTypeface());
        text_back.setBackgroundColor(Color.TRANSPARENT);
        
        text_back.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				finish();
			}
		});

        
        adapter = new StableArrayAdapter(this, R.layout.achievement_list_item_1, eventManager.getAchievementsLabelList());
        
        listview.setAdapter(adapter);
        listview.setDivider(null);
        listview.setVerticalFadingEdgeEnabled(false);
        listview.setScrollbarFadingEnabled(false);
		
    }
    
    
    
    private class StableArrayAdapter extends ArrayAdapter<String> {

    	HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();
	
    	
        public StableArrayAdapter(Context context, int textViewResourceId, List<String> objects) {
        	super(context, textViewResourceId, objects);
          
        	for (int i = 0; i < objects.size(); i++) 
        		mIdMap.put(objects.get(i), i);
        }

        
        
        
    	public View getView(int position, View convertView, ViewGroup parent) {
    	
    		View view = super.getView(position, convertView, parent);
    		TextView textView = (TextView) view.findViewById(R.id.achievement_item); 
    		textView.setTypeface(HardPartLoader.tf);
    		textView.setTextSize(textsize/4);
    		textView.setShadowLayer(0.6f, 1.5f, 1.5f, getResources().getColor(R.color.shadow_color));
    		
    		if (achivementsSet.contains(position)) textView.setTextColor(getResources().getColor(R.color.plain_string)); 
    			else textView.setTextColor(getResources().getColor(R.color.plain_string_alpha));
          
    		return textView;        
    	}	
    	
        
    	
        @Override
        public long getItemId(int position) {
        	
        	String item = getItem(position);
        	return mIdMap.get(item);
        	
        }

        
        
        @Override
        public boolean isEnabled(int position){
        	
        	return false;

        }
        
        
        @Override
        public boolean areAllItemsEnabled(){
        	
        	return false;
        	
        }
        
        
        @Override
        public boolean hasStableIds() {
        	
        	return true;
        	
        }

      }
    

    
}