package com.alexgolikov.getloaded;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

import android.util.Log;


public class Player {
	
	//maximum possible intoxication points
	private static final float INTOXICATION_LIMIT = 1000; //1000 is right, another just for test	
	
	//massive of drank drinks during the game
	private ArrayList<Drink> drankedMass;
	private ArrayList<Integer> timeMass;
	
	EventManager eventManager;
	
	public String name;
	
	private float intoxicationLevel = 0;
	private int points = 0;
	private float lastIntoxication = 0;

	private boolean can_drink;
	
	
	Player(EventManager eventManager){
		
		drankedMass = new ArrayList<Drink>();
		timeMass = new ArrayList<Integer>();
		
		this.eventManager = eventManager;
		can_drink = true;
	}
	

	public void setIntoxicationLevel(Drink drink, int time){
			
		if (drink.isDrink()){
			addDrinkToDrankMass(drink, time);
			lastIntoxication = GameModel.calculateStrength(drink);
			intoxicationLevel += lastIntoxication;
			
			if (intoxicationLevel >= INTOXICATION_LIMIT){
				GameModel.setGameState(GameModel.STATE_LOSE);
				GUI.finalScreen.createFinalScreen(points);
				can_drink = false;			
			}
		}
		
	}
	
	
	private void addDrinkToDrankMass(Drink drink, int time){	
		
		if (drankedMass.size() == GameModel.DRINKS_COUNT_FOR_ACHIEVEMENT_CHECK + 1){
			for (int i = drankedMass.size() - 1; i > 0; i--){
				drankedMass.set(i, drankedMass.get(i - 1));
				timeMass.set(i, timeMass.get(i - 1));
			}
			
			drankedMass.set(0, drink);
			timeMass.set(0, time);
		} 
			else {
				drankedMass.add(0, drink);
				timeMass.add(0, time);
			}
		
		eventManager.checkPoints(points);
		eventManager.checkAchievmentEvent(drankedMass, timeMass);
		eventManager.checkProDrinkerEvent(drankedMass.get(0));
	}
	

	public ArrayList<Drink> getMass(){
		return drankedMass;
	}
	
	public ArrayList<Integer> getTimeMass(){
		return timeMass;
	}
	
	public void clearTimeMass(){
		timeMass.clear();
	}
	
	public Drink getDrinkFromMass(int index){
		return drankedMass.get(index);
	}
	
	public float getIntoxicationLimit(){
		return INTOXICATION_LIMIT;
	}
	
	
	public float getLastIntoxication(){
		return lastIntoxication;
	}
	
	
	public void setPoints(int points){
		this.points += points;
		if (this.points < 0) this.points = 0;
	}
	
	
	public boolean isCanDrink(){
		return can_drink;
	}
	
	
	public void setCanDrink(boolean status){
		can_drink = status;
	}
	
	
	public float getIntoxicationLevel(){
		return intoxicationLevel;
	}
	
	
	public void clearDrankMass(){
		drankedMass.clear();
	}
	
	
	public int getPoints(){
		return points;
	}
	

}
