package com.alexgolikov.getloaded;


import android.graphics.Bitmap;


public class Drink{
	
    public static final int STATE_SLIDE = -1;
    public static final int STATE_HOLD = 0;
    public static final int STATE_DRINK = 1;
    public static final int STATE_RETURN = 2;
    
    public static final int VODKA_DRINK = 1;
    public static final int BEER_DRINK = 2;
    public static final int COGNAC_DRINK = 3;
    public static final int ABSINTHE_DRINK = 4;
    public static final int B52_DRINK = 5;
    public static final int BLOOD_MARY_DRINK = 6;
    public static final int CHAMPAGNE_DRINK = 7;
    public static final int LONG_ISLAND_DRINK = 8;
    public static final int MARGARITA_DRINK = 9;
    public static final int MOJITO_DRINK = 10;
    public static final int TEQUILA_DRINK = 11;
    public static final int WINE_DRINK = 12;
    public static final int UNKNOWN_DRINK = 13;
    public static final int POINTS_100_DRINK = 14;
    public static final int POINTS_250_DRINK = 15;
    public static final int POINTS_500_DRINK = 16;
    
    public static final int SECRET_DRINK_1 = 100;
    public static final String SECRET_DRINK_1_NAME = "Butter Beer";
    
    public volatile int currentState = STATE_SLIDE;

	
    protected int id = 0;
    
    //variables for animation
    private int animation_frames = 0;
    private int animation_delay = 0;
    private int animation_delay_pointer = 0;
    private int current_frame = 0;
    private int delay_frame = 0; 
    
	public float strengthOfDrink = 0; 
	public int pointsForDrink = 0;
	
	//coordinates of moving object
	public float position_x = 0;
	public float position_y = 0;
	private float position_x_original = 0;
	private float position_y_original = 0;
	
	//coordinates of holding object
	public volatile float holding_x = 0;
	public volatile float holding_y = 0;
	
	//boolean for separate drinks and bonuses
	private boolean isDrink = true;
	//boolean for secret drink
	private boolean isSecret = false;
	
	Bitmap[] drinkImage;
	
	public int imageWidth = 0;
	public int imageHeight = 0;
	
	private String name;
	private String description;
	
	public Drink(int x, int y, Bitmap[] drinkImage){

		this.position_x = x;
		this.position_y = y;
		position_x_original = x;
		position_y_original = y;
		this.holding_x = x;
		this.holding_y = y;
		this.drinkImage = drinkImage;
		currentState = STATE_SLIDE;
		
		imageWidth = drinkImage[0].getWidth();
		imageHeight = drinkImage[0].getHeight();

	}

	
	public void resetState(){
		position_x = position_x_original;
		position_y = position_y_original;
		setCurrentState(STATE_SLIDE);
	}
	
	
	public float getStrength(){
		return strengthOfDrink;
	}
	
	public int getPoints(){
		return pointsForDrink;
	}
	
	public int getCurrentState(){
		return currentState;
	}
	
	public Bitmap getDrinkImageBlank(){
		return drinkImage[0];
	}
	
	
	public int getAnimationDelay(){
		return animation_delay;
	}
	
	public int getImageWidth(){
		return imageWidth;
	}
	
	public int getImageHeight(){
		return imageHeight;
	}
	
	public void setCurrentState(int state){
		this.currentState = state;
	}
	
	
	public void setHoldingPosition(float x, float y){
		this.holding_x = x;
		this.holding_y = y;
	}
	
	
	public void setHoldingX(float x){
		this.holding_x = x;
	}
	
	
	public void setHoldingY(float y){
		this.holding_y = y;
	}
	
	
	
	public int getFrame(){
		if (current_frame <= delay_frame) return 0; else return (current_frame - delay_frame);
	}
	
	
	
	public void changeFrame(){
		if (animation_delay > 0){
			if (--animation_delay_pointer < 0){
				animation_delay_pointer = animation_delay;
				if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
			}
		} else if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
	}
	
	
	
	public boolean isDrink(){
		return isDrink;
	}
	
	
	public int getId(){
		return id;
	}
	
	public boolean isSecretDrink(){
		return isSecret;
	}
	
	public String getName(){
		return name;
	}
	
	
	public String getDescription(){
		return description;
	}
	
	
	public Bitmap getBitmap(){
		return drinkImage[0];
	}
	

	
}



class VodkaDrink extends Drink{
	
	public int id = 1;
	
	public float strengthOfDrink = 50;
	public int pointsForDrink = 420;
	
    private int animation_frames = 0;
    private int animation_delay = 0;
    private int animation_delay_pointer = 0;
    private int current_frame = 0;
    private int delay_frame = 0; 
    
	private boolean isDrink = true;
	private boolean isSecret = false;
	
	private String name = "Vodka"; 
	private String description = "Shot of distilled beverage, composed primarily of water and ethanol. Strong greetings from Easter Europe!";
	
	public VodkaDrink(int x, int y, Bitmap[] drinkImage){
		super(x, y, drinkImage);
	}
	
	
	public float getStrength(){
		return strengthOfDrink;
	}
	
	
	public int getPoints(){
		return pointsForDrink;
	}
	
	
	public int getAnimationDelay(){
		return animation_delay;
	}
	
	
	public int getFrame(){
		if (current_frame <= delay_frame) return 0; else return (current_frame - delay_frame);
	}
	
	
	public void changeFrame(){
		if (animation_delay > 0){
			if (--animation_delay_pointer < 0){
				animation_delay_pointer = animation_delay;
				if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
			}
		} else if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
	}
	
	public boolean isSecretDrink(){
		return isSecret;
	}
	
	public int getId(){
		return id;
	}
	
	
	public boolean isDrink(){
		return isDrink;
	}
	
	
	public String getName(){
		return name;
	}
	
	public String getDescription(){
		return description;
	}
	
}




class BeerDrink extends Drink{
	
	public int id = 2;
	
	public float strengthOfDrink = 5;
	public int pointsForDrink = 50;
	
    private int animation_frames = 4;
    private int animation_delay = 1;
    private int animation_delay_pointer = 1;
    private int current_frame = 0;
    private int delay_frame = 0; 
    
	private boolean isDrink = true;
	private boolean isSecret = false;
	
	private String name = "Beer";
	private String description = "The world's most widely consumed alcoholic beverage, produced by the saccharification of starch and fermentation of the resulting sugar";
	
	public BeerDrink(int x, int y, Bitmap[] drinkImage){
		super(x, y, drinkImage);
	}
	
	public float getStrength(){
		return strengthOfDrink;
	}
	
	
	public int getPoints(){
		return pointsForDrink;
	}
	
	public int getAnimationDelay(){
		return animation_delay;
	}
	
	
	public int getFrame(){
		if (current_frame <= delay_frame) return 0; else return (current_frame - delay_frame);
	}
	
	
	public void changeFrame(){
		if (animation_delay > 0){
			if (--animation_delay_pointer < 0){
				animation_delay_pointer = animation_delay;
				if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
			}
		} else if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
	}
	
	public boolean isSecretDrink(){
		return isSecret;
	}
	
	public int getId(){
		return id;
	}
	
	public boolean isDrink(){
		return isDrink;
	}
	
	public String getName(){
		return name;
	}
	
	public String getDescription(){
		return description;
	}
}
	
	

	
class CognacDrink extends Drink{
		
	public int id = 3;
		
	public float strengthOfDrink = 40;
	public int pointsForDrink = 300;
		
	private int animation_frames = 5;
	private int animation_delay = 1;
	private int animation_delay_pointer = 1;
	private int current_frame = 0;
	private int delay_frame = 10; 
	
	private boolean isDrink = true;
	private boolean isSecret = false;
	
	private String name = "Cognac";
	private String description = "Cognac is a type of brandy, produced by distilling wine, named after the town of Cognac in France. Generally contains 40% alcohol by volume (abv) and is typically taken as an after-dinner drink";
		
	public CognacDrink(int x, int y, Bitmap[] drinkImage){
		super(x, y, drinkImage);
	}
		
	public float getStrength(){
		return strengthOfDrink;
	}
		
	public int getPoints(){
		return pointsForDrink;
	}
	
	public int getAnimationDelay(){
		return animation_delay;
	}
		
	public int getFrame(){
		if (current_frame <= delay_frame) return 0; else return (current_frame - delay_frame);
	}
		
	public void changeFrame(){
		if (animation_delay > 0){
			if (--animation_delay_pointer < 0){
				animation_delay_pointer = animation_delay;
				if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
			}
		} else if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
	}
		
	public int getId(){
		return id;
	}
	
	public boolean isSecretDrink(){
		return isSecret;
	}
	
	public boolean isDrink(){
		return isDrink;
	}
	
	public String getName(){
		return name;
	}
	
	public String getDescription(){
		return description;
	}
	
}



class AbsintheDrink extends Drink{
	
	public int id = 4;
		
	public float strengthOfDrink = 60;
	public int pointsForDrink = 500;
		
	private int animation_frames = 0;
	private int animation_delay = 0;
	private int animation_delay_pointer = 0;
	private int current_frame = 0;
	private int delay_frame = 0; 
	
	private boolean isDrink = true;
	private boolean isSecret = false;
	
	private String name = "Absinthe";
	private String description = "";
		
	public AbsintheDrink(int x, int y, Bitmap[] drinkImage){
		super(x, y, drinkImage);
	}
		
	public float getStrength(){
		return strengthOfDrink;
	}
		
	public int getPoints(){
		return pointsForDrink;
	}
	
	public int getAnimationDelay(){
		return animation_delay;
	}
		
	public int getFrame(){
		if (current_frame <= delay_frame) return 0; else return (current_frame - delay_frame);
	}
		
	public void changeFrame(){
		if (animation_delay > 0){
			if (--animation_delay_pointer < 0){
				animation_delay_pointer = animation_delay;
				if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
			}
		} else if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
	}
		
	public int getId(){
		return id;
	}
	
	public boolean isSecretDrink(){
		return isSecret;
	}
	
	public boolean isDrink(){
		return isDrink;
	}
	
	public String getName(){
		return name;
	}
	
	public String getDescription(){
		return description;
	}
	
}




class B52Drink extends Drink{
	
	public int id = 5;
		
	public float strengthOfDrink = 27;
	public int pointsForDrink = 190;
		
	private int animation_frames = 5;
	private int animation_delay = 4;
	private int animation_delay_pointer = 4;
	private int current_frame = 0;
	private int delay_frame = 0; 
	
	private boolean isDrink = true;
	private boolean isSecret = false;
	
	private String name = "B 52";
	private String description = "";
		
	public B52Drink(int x, int y, Bitmap[] drinkImage){
		super(x, y, drinkImage);
	}
		
	public float getStrength(){
		return strengthOfDrink;
	}
		
	public int getPoints(){
		return pointsForDrink;
	}
	
	public int getAnimationDelay(){
		return animation_delay;
	}
		
	public int getFrame(){
		if (current_frame <= delay_frame) return 0; else return (current_frame - delay_frame);
	}
		
	public void changeFrame(){
		if (animation_delay > 0){
			if (--animation_delay_pointer < 0){
				animation_delay_pointer = animation_delay;
				if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
			}
		} else if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
	}
		
	public int getId(){
		return id;
	}
	
	public boolean isSecretDrink(){
		return isSecret;
	}
	
	public boolean isDrink(){
		return isDrink;
	}
	
	public String getName(){
		return name;
	}
	
	public String getDescription(){
		return description;
	}
	
}


	
class BloodMaryDrink extends Drink{
	
	public int id = 6;
		
	public float strengthOfDrink = 12;
	public int pointsForDrink = 100;
		
	private int animation_frames = 4;
	private int animation_delay = 3;
	private int animation_delay_pointer = 3;
	private int current_frame = 0;
	private int delay_frame = 0; 
	
	private boolean isDrink = true;
	private boolean isSecret = false;
	
	private String name = "Bloody Mary";
	private String description = "";
		
	public BloodMaryDrink(int x, int y, Bitmap[] drinkImage){
		super(x, y, drinkImage);
	}
		
	public float getStrength(){
		return strengthOfDrink;
	}
		
	public int getPoints(){
		return pointsForDrink;
	}
	
	public int getAnimationDelay(){
		return animation_delay;
	}
		
	public int getFrame(){
		if (current_frame <= delay_frame) return 0; else return (current_frame - delay_frame);
	}
		
	public void changeFrame(){
		if (animation_delay > 0){
			if (--animation_delay_pointer < 0){
				animation_delay_pointer = animation_delay;
				if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
			}
		} else if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
	}
		
	public int getId(){
		return id;
	}
	
	
	public boolean isDrink(){
		return isDrink;
	}
	
	public boolean isSecretDrink(){
		return isSecret;
	}
	
	public String getName(){
		return name;
	}
	
	public String getDescription(){
		return description;
	}
	
}


class ChampagneDrink extends Drink{
	
	public int id = 7;
		
	public float strengthOfDrink = 18;
	public int pointsForDrink = 135;
		
	private int animation_frames = 4;
	private int animation_delay = 1;
	private int animation_delay_pointer = 1;
	private int current_frame = 0;
	private int delay_frame = 0; 
	
	private boolean isDrink = true;
	private boolean isSecret = false;
	
	private String name = "Champagne";
	private String description = "";
		
	public ChampagneDrink(int x, int y, Bitmap[] drinkImage){
		super(x, y, drinkImage);
	}
		
	public float getStrength(){
		return strengthOfDrink;
	}
		
	public int getPoints(){
		return pointsForDrink;
	}
	
	public int getAnimationDelay(){
		return animation_delay;
	}
		
	public int getFrame(){
		if (current_frame <= delay_frame) return 0; else return (current_frame - delay_frame);
	}
		
	public void changeFrame(){
		if (animation_delay > 0){
			if (--animation_delay_pointer < 0){
				animation_delay_pointer = animation_delay;
				if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
			}
		} else if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
	}
		
	public int getId(){
		return id;
	}
	
	
	public boolean isDrink(){
		return isDrink;
	}
	
	public String getName(){
		return name;
	}
	
	public boolean isSecretDrink(){
		return isSecret;
	}
	
	public String getDescription(){
		return description;
	}
	
}



class LongIslandDrink extends Drink{
	
	public int id = 8;
		
	public float strengthOfDrink = 22;
	public int pointsForDrink = 160;
		
	private int animation_frames = 4;
	private int animation_delay = 2;
	private int animation_delay_pointer = 2;
	private int current_frame = 0;
	private int delay_frame = 0; 
	
	private boolean isDrink = true;
	private boolean isSecret = false;
	
	private String name = "Long Island";
	private String description = "";
		
	public LongIslandDrink(int x, int y, Bitmap[] drinkImage){
		super(x, y, drinkImage);
	}
		
	public float getStrength(){
		return strengthOfDrink;
	}
		
	public int getPoints(){
		return pointsForDrink;
	}
	
	public int getAnimationDelay(){
		return animation_delay;
	}
		
	public int getFrame(){
		if (current_frame <= delay_frame) return 0; else return (current_frame - delay_frame);
	}
		
	public void changeFrame(){
		if (animation_delay > 0){
			if (--animation_delay_pointer < 0){
				animation_delay_pointer = animation_delay;
				if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
			}
		} else if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
	}
		
	public int getId(){
		return id;
	}
	
	
	public boolean isDrink(){
		return isDrink;
	}
	
	public boolean isSecretDrink(){
		return isSecret;
	}
	
	public String getName(){
		return name;
	}
	
	public String getDescription(){
		return description;
	}
	
}


class MargaritaDrink extends Drink{
	
	public int id = 9;
		
	public float strengthOfDrink = 35;
	public int pointsForDrink = 240;
		
	private int animation_frames = 4;
	private int animation_delay = 3;
	private int animation_delay_pointer = 3;
	private int current_frame = 0;
	private int delay_frame = 0; 
	
	private boolean isDrink = true;
	private boolean isSecret = false;
	
	private String name = "Margarita";
	private String description = "";
		
	public MargaritaDrink(int x, int y, Bitmap[] drinkImage){
		super(x, y, drinkImage);
	}
		
	public float getStrength(){
		return strengthOfDrink;
	}
		
	public int getPoints(){
		return pointsForDrink;
	}
	
	public int getAnimationDelay(){
		return animation_delay;
	}
		
	public int getFrame(){
		if (current_frame <= delay_frame) return 0; else return (current_frame - delay_frame);
	}
		
	public void changeFrame(){
		if (animation_delay > 0){
			if (--animation_delay_pointer < 0){
				animation_delay_pointer = animation_delay;
				if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
			}
		} else if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
	}
		
	public int getId(){
		return id;
	}
	
	
	public boolean isDrink(){
		return isDrink;
	}
	
	public boolean isSecretDrink(){
		return isSecret;
	}
	
	public String getName(){
		return name;
	}
	
	public String getDescription(){
		return description;
	}
	
}



class MojitoDrink extends Drink{
	
	public int id = 10;
		
	public float strengthOfDrink = 9;
	public int pointsForDrink = 85;
		
	private int animation_frames = 4;
	private int animation_delay = 3;
	private int animation_delay_pointer = 3;
	private int current_frame = 0;
	private int delay_frame = 0; 
	
	private boolean isDrink = true;
	private boolean isSecret = false;
	
	private String name = "Mojito";
	private String description = "";
		
	public MojitoDrink(int x, int y, Bitmap[] drinkImage){
		super(x, y, drinkImage);
	}
		
	public float getStrength(){
		return strengthOfDrink;
	}
		
	public int getPoints(){
		return pointsForDrink;
	}
	
	public int getAnimationDelay(){
		return animation_delay;
	}
		
	public int getFrame(){
		if (current_frame <= delay_frame) return 0; else return (current_frame - delay_frame);
	}
		
	public void changeFrame(){
		if (animation_delay > 0){
			if (--animation_delay_pointer < 0){
				animation_delay_pointer = animation_delay;
				if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
			}
		} else if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
	}
		
	public int getId(){
		return id;
	}
	
	
	public boolean isDrink(){
		return isDrink;
	}
	
	public boolean isSecretDrink(){
		return isSecret;
	}
	
	public String getName(){
		return name;
	}
	
	public String getDescription(){
		return description;
	}
	
}



class TequilaDrink extends Drink{
	
	public int id = 11;
		
	public float strengthOfDrink = 45;
	public int pointsForDrink = 360;
		
	private int animation_frames = 0;
	private int animation_delay = 0;
	private int animation_delay_pointer = 0;
	private int current_frame = 0;
	private int delay_frame = 0; 
	
	private boolean isDrink = true;
	private boolean isSecret = false;
	
	private String name = "Tequila";
	private String description = "";
		
	public TequilaDrink(int x, int y, Bitmap[] drinkImage){
		super(x, y, drinkImage);
	}
		
	public float getStrength(){
		return strengthOfDrink;
	}
		
	public int getPoints(){
		return pointsForDrink;
	}
	
	public int getAnimationDelay(){
		return animation_delay;
	}
		
	public int getFrame(){
		if (current_frame <= delay_frame) return 0; else return (current_frame - delay_frame);
	}
		
	public void changeFrame(){
		if (animation_delay > 0){
			if (--animation_delay_pointer < 0){
				animation_delay_pointer = animation_delay;
				if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
			}
		} else if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
	}
		
	public int getId(){
		return id;
	}
	
	
	public boolean isDrink(){
		return isDrink;
	}
	
	public boolean isSecretDrink(){
		return isSecret;
	}
	
	public String getName(){
		return name;
	}
	
	public String getDescription(){
		return description;
	}
	
}



class WineDrink extends Drink{
	
	public int id = 12;
		
	public float strengthOfDrink = 15;
	public int pointsForDrink = 120;
		
	private int animation_frames = 5;
	private int animation_delay = 1;
	private int animation_delay_pointer = 1;
	private int current_frame = 0;
	private int delay_frame = 10; 
	
	private boolean isDrink = true;
	private boolean isSecret = false;
	
	private String name = "Wine";
	private String description = "";
		
	public WineDrink(int x, int y, Bitmap[] drinkImage){
		super(x, y, drinkImage);
	}
		
	public float getStrength(){
		return strengthOfDrink;
	}
		
	public int getPoints(){
		return pointsForDrink;
	}
	
	public int getAnimationDelay(){
		return animation_delay;
	}
		
	public int getFrame(){
		if (current_frame <= delay_frame) return 0; else return (current_frame - delay_frame);
	}
		
	public void changeFrame(){
		if (animation_delay > 0){
			if (--animation_delay_pointer < 0){
				animation_delay_pointer = animation_delay;
				if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
			}
		} else if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
	}
		
	public int getId(){
		return id;
	}
	
	
	public boolean isDrink(){
		return isDrink;
	}
	
	public boolean isSecretDrink(){
		return isSecret;
	}
	
	public String getName(){
		return name;
	}
	
	public String getDescription(){
		return description;
	}
	
}



class UnknownDrink extends Drink{
	
	public int id = 13;
		
	public float strengthOfDrink = 0;
	public int pointsForDrink = 0;
		
	private int animation_frames = 4;
	private int animation_delay = 3;
	private int animation_delay_pointer = 3;
	private int current_frame = 0;
	private int delay_frame = 0; 
	
	private boolean isDrink = true;
	private boolean isSecret = false;
	
	private String name = "Unknown Drink";
	private String description = "";
		
	public UnknownDrink(int x, int y, Bitmap[] drinkImage){
		super(x, y, drinkImage);
	}
		
	public float getStrength(){
		return strengthOfDrink;
	}
		
	public int getPoints(){
		return pointsForDrink;
	}
	
	public int getAnimationDelay(){
		return animation_delay;
	}
		
	public int getFrame(){
		if (current_frame <= delay_frame) return 0; else return (current_frame - delay_frame);
	}
		
	public void changeFrame(){
		if (animation_delay > 0){
			if (--animation_delay_pointer < 0){
				animation_delay_pointer = animation_delay;
				if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
			}
		} else if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
	}
		
	public int getId(){
		return id;
	}
	
	
	public boolean isDrink(){
		return isDrink;
	}
	
	public boolean isSecretDrink(){
		return isSecret;
	}
	
	public String getName(){
		return name;
	}
	
	public String getDescription(){
		return description;
	}
	
}




class Points100Drink extends Drink{
		
	public int id = 14;
			
	public float strengthOfDrink = 0;
	public int pointsForDrink = 100;
			
	private int animation_frames = 5;
	private int animation_delay = 2;
	private int animation_delay_pointer = 2;
	private int current_frame = 0;
	private int delay_frame = 10; 
		
	private boolean isDrink = false;
	private boolean isSecret = false;
	
	private String name = "Extra 100 points";
	private String description = "Non-alcoholic drink, that gives additional points and doesn't influence to your condition";
			
	public Points100Drink(int x, int y, Bitmap[] drinkImage){
		super(x, y, drinkImage);
	}
			
	public float getStrength(){
		return strengthOfDrink;
	}
			
	public int getPoints(){
		return pointsForDrink;
	}
	
	public int getAnimationDelay(){
		return animation_delay;
	}
			
	public int getFrame(){
		if (current_frame <= delay_frame) return 0; else return (current_frame - delay_frame);
	}
			
	public void changeFrame(){
		if (animation_delay > 0){
			if (--animation_delay_pointer < 0){
				animation_delay_pointer = animation_delay;
				if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
			}
		} else if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
	}
			
	public int getId(){
		return id;
	}
		
		
	public boolean isDrink(){
		return isDrink;
	}
	
	public boolean isSecretDrink(){
		return isSecret;
	}
	
	public String getName(){
		return name;
	}
	
	public String getDescription(){
		return description;
	}
}
	


class Points250Drink extends Drink{
	
	public int id = 15;
			
	public float strengthOfDrink = 0;
	public int pointsForDrink = 250;
			
	private int animation_frames = 5;
	private int animation_delay = 2;
	private int animation_delay_pointer = 2;
	private int current_frame = 0;
	private int delay_frame = 10; 
		
	private boolean isDrink = false;
	private boolean isSecret = false;
	
	private String name = "Extra 250 points";
	private String description = "Non-alcoholic drink, that gives additional points and doesn't influence to your condition";
			
	public Points250Drink(int x, int y, Bitmap[] drinkImage){
		super(x, y, drinkImage);
	}
			
	public float getStrength(){
		return strengthOfDrink;
	}
			
	public int getPoints(){
		return pointsForDrink;
	}
	
	public int getAnimationDelay(){
		return animation_delay;
	}
			
	public int getFrame(){
		if (current_frame <= delay_frame) return 0; else return (current_frame - delay_frame);
	}
			
	public void changeFrame(){
		if (animation_delay > 0){
			if (--animation_delay_pointer < 0){
				animation_delay_pointer = animation_delay;
				if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
			}
		} else if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
	}
			
	public int getId(){
		return id;
	}
		
	
	public boolean isSecretDrink(){
		return isSecret;
	}
		
	public boolean isDrink(){
		return isDrink;
	}
	
	public String getName(){
		return name;
	}
	
	public String getDescription(){
		return description;
	}
}



class Points500Drink extends Drink{
	
	public int id = 16;
			
	public float strengthOfDrink = 0;
	public int pointsForDrink = 500;
			
	private int animation_frames = 5;
	private int animation_delay = 2;
	private int animation_delay_pointer = 2;
	private int current_frame = 0;
	private int delay_frame = 10; 
		
	private boolean isDrink = false;
	private boolean isSecret = false;
	
	private String name = "Extra 500 points";
	private String description = "Non-alcoholic drink, that gives additional points and doesn't influence to your condition";
			
	public Points500Drink(int x, int y, Bitmap[] drinkImage){
		super(x, y, drinkImage);
	}
			
	public float getStrength(){
		return strengthOfDrink;
	}
			
	public int getPoints(){
		return pointsForDrink;
	}
	
	public int getAnimationDelay(){
		return animation_delay;
	}
			
	public int getFrame(){
		if (current_frame <= delay_frame) return 0; else return (current_frame - delay_frame);
	}
			
	public void changeFrame(){
		if (animation_delay > 0){
			if (--animation_delay_pointer < 0){
				animation_delay_pointer = animation_delay;
				if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
			}
		} else if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
	}
			
	public int getId(){
		return id;
	}
		
		
	public boolean isDrink(){
		return isDrink;
	}
	
	public String getName(){
		return name;
	}
	
	public boolean isSecretDrink(){
		return isSecret;
	}
	
	public String getDescription(){
		return description;
	}
}



class ButterBeer extends Drink{
	
	public int id = 100;
			
	public float strengthOfDrink = 5;
	public int pointsForDrink = 1000;
			
	private int animation_frames = 0;
	private int animation_delay = 0;
	private int animation_delay_pointer = 0;
	private int current_frame = 0;
	private int delay_frame = 0; 
		
	private boolean isDrink = true;
	private boolean isSecret = true;
	
	private double price_ue = 0.5d;
	private String name_hide = "Secret Drink";
	private String name = SECRET_DRINK_1_NAME;
	private String description = "Best beer in Hogwards";
	
	private Bitmap[] drinkImageBlank;
			
	public ButterBeer(int x, int y, Bitmap[] drinkImage, Bitmap[] drinkImageBlank){
		super(x, y, drinkImage);
		this.drinkImageBlank = drinkImageBlank;
	}
	
	public Bitmap getDrinkImageBlank(){
		return drinkImageBlank[0];
	}
			
	public float getStrength(){
		return strengthOfDrink;
	}
			
	public int getPoints(){
		return pointsForDrink;
	}
	
	public int getAnimationDelay(){
		return animation_delay;
	}
			
	
	public String getNameHide(){
		return name_hide;
	}
	
	public int getFrame(){
		if (current_frame <= delay_frame) return 0; else return (current_frame - delay_frame);
	}
			
	public void changeFrame(){
		if (animation_delay > 0){
			if (--animation_delay_pointer < 0){
				animation_delay_pointer = animation_delay;
				if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
			}
		} else if (++current_frame > (animation_frames + delay_frame)) current_frame = 0;
	}
			
	public int getId(){
		return id;
	}
		
	
	public boolean isSecretDrink(){
		return isSecret;
	}
		
	public boolean isDrink(){
		return isDrink;
	}
	
	public String getName(){
		return name;
	}
	
	public String getDescription(){
		return description;
	}
}


